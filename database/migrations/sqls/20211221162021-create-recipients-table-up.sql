/* Replace with your SQL commands */
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE recipient(
    id VARCHAR PRIMARY KEY DEFAULT 'recipient-' || LOWER(
        REPLACE(
            CAST(uuid_generate_v1mc() As varchar(50))
            , '-','')
        ),
    recipient_name VARCHAR NOT NULL,
    bank_name VARCHAR NOT NULL,
    bank_code VARCHAR NOT NULL,
    account_name VARCHAR NOT NULL,
    currency VARCHAR,
    account_number VARCHAR NOT NULL,
    recipient_code VARCHAR DEFAULT NULL,
    deleted_at TIMESTAMPTZ DEFAULT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);