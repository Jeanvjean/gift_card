/* Replace with your SQL commands */

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE gift_cards(
    id VARCHAR PRIMARY KEY DEFAULT 'gift_card-' || LOWER(
        REPLACE(
            CAST(uuid_generate_v1mc() As varchar(50))
            , '-','')
        ),
    amount VARCHAR NOT NULL,
    user_id VARCHAR NOT NULL,
    currency VARCHAR,
    transaction_id VARCHAR NOT NULL,
    redeem_link VARCHAR NOT NULL,
    redeem_token VARCHAR NOT NULL,
    redeem_status VARCHAR NOT NULL,
    deleted_at TIMESTAMPTZ DEFAULT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);