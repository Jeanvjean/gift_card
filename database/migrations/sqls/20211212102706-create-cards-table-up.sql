/* Replace with your SQL commands */

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE cards(
    id VARCHAR PRIMARY KEY DEFAULT 'card-' || LOWER(
        REPLACE(
            CAST(uuid_generate_v1mc() As varchar(50))
            , '-','')
        ),
    last_4_digits VARCHAR NOT NULL,
    card_type VARCHAR NOT NULL,
    authorization_code VARCHAR NOT NULL,
    exp_year VARCHAR NOT NULL,
    exp_month VARCHAR NOT NULL,
    holder_name VARCHAR NOT NULL,
    default_card VARCHAR NOT NULL,
    user_id VARCHAR NOT NULL,
    transactions INTEGER NOT NULL,
    deleted_at TIMESTAMPTZ DEFAULT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);