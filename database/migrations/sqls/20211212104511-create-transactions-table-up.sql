/* Replace with your SQL commands */

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
CREATE TABLE transactions(
    id VARCHAR PRIMARY KEY DEFAULT 'transaction-' || LOWER(
        REPLACE(
            CAST(uuid_generate_v1mc() As varchar(50))
            , '-','')
        ),
    source VARCHAR NOT NULL,
    amount VARCHAR NOT NULL,
    user_id VARCHAR NOT NULL,
    currency VARCHAR,
    transaction_type VARCHAR NOT NULL,
    transaction_date TIMESTAMPTZ DEFAULT NULL,
    service VARCHAR NOT NULL,
    transaction_reference VARCHAR NOT NULL,
    transaction_status VARCHAR NOT NULL,
    deleted_at TIMESTAMPTZ DEFAULT NULL,
    created_at TIMESTAMPTZ DEFAULT NOW(),
    updated_at TIMESTAMPTZ DEFAULT NOW()
);