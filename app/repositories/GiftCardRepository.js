const BaseRepository = require('./BaseRepository');
class GiftCardRepository extends BaseRepository {
  constructor({ config, errors, database, queries, helper }) {
    super({
      config,
      errors,
      database,
      queries,
      helper,
      tableName: 'gift_cards',
    });
		this.config = config;
		this.errors = errors;
		this.database = database;
		this.queries = queries;
		this.helper = helper;
	}
}
module.exports = GiftCardRepository;
