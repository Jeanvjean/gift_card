const BaseRepository = require('./BaseRepository');
class ApplicantCustomValueRepository extends BaseRepository {
  constructor({ config, errors, database, queries, helper }) {
    super({
      config,
      errors,
      database,
      queries,
      helper,
      tableName: 'applicant_custom_value',
    });
		this.config = config;
		this.errors = errors;
		this.database = database;
		this.queries = queries;
		this.helper = helper;
	}
}
module.exports = ApplicantCustomValueRepository
