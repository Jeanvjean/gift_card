const BaseRepository = require('./BaseRepository');
class UserRepository extends BaseRepository {
  constructor({ config, errors, database, queries, helper }) {
    super({
      config,
      errors,
      database,
      queries,
      helper,
      tableName: 'users',
    });
		this.config = config;
		this.errors = errors;
		this.database = database;
		this.queries = queries;
		this.helper = helper;
	}

    async fetchUserById({id}){
        const [user] = await this.database.query(this.queries.users.selectById, {
            id,
        });
        return user;
    }

    async fetchUserByEmail({email}) {
      const [user] = await this.database.query(this.queries.users.selectByEmail, {
        email,
      })
      return user
    }
}
module.exports = UserRepository
