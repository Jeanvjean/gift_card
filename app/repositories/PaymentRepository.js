const BaseRepository = require('./BaseRepository');

class PaymentRepository extends BaseRepository {
    constructor({ config, errors, database, queries, helper }) {
      super({
        config,
        errors,
        database,
        queries,
        helper,
        tableName: 'cards',
      });
          this.config = config;
          this.errors = errors;
          this.database = database;
          this.queries = queries;
          this.helper = helper;
      }
  }

  module.exports = PaymentRepository;