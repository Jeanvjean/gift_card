const BaseRepository = require('./BaseRepository');
class GiftCardTypesRepository extends BaseRepository {
  constructor({ config, errors, database, queries, helper }) {
    super({
      config,
      errors,
      database,
      queries,
      helper,
      tableName: 'gift_cards_type',
    });
		this.config = config;
		this.errors = errors;
		this.database = database;
		this.queries = queries;
		this.helper = helper;
	}
}
module.exports = GiftCardTypesRepository;
