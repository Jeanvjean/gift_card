class BaseRepository {
  constructor({ config, errors, database, queries, helper, tableName }) {
    this.config = config;
    this.errors = errors;
    this.database = database;
    this.queries = queries;
    this.helper = helper;
    this.tableName = tableName;
    this.conditionKeys = [
      '_eq',
      '_neq',
      '_gt',
      '_gte',
      '_lt',
      '_lte',
      '_nnull',
      '_null',
      '_like',
      '_ilike',
    ];
    this.logicKey = ['_and', '_or'];
  }

  async baseInsert({ data, transaction }) {
    if (transaction) {
      return transaction.any(this.buildInsertQuery(data), data);
    }
    const response = await this.database.query(
      this.buildInsertQuery(data),
      data
    );
    return response;
  }

  async baseUpsert({ data, where }) {
    const exist = await this.baseGetOne({ where, withSoftDelete: true });
    if (exist) {
      const [insert] = await this.baseUpdate({
        where: { id: { _eq: exist.id } },
        _set: data,
      });

      return insert;
    }
    return await this.baseInsert({ data });
  }

  async baseGetAll(
    {
      columns = '*',
      where = {},
      join = [],
      withSoftDelete = false,
      page,
      orderBy,
      groupBy,
      transaction,
      limit,
      transactionCallback = (all) => all,
    } = {
      withSoftDelete: false,
    }
  ) {
    const getLimitOffset = this.getOffsetValue({ page });

    const query = this.buildSelectQuery({
      columns,
      where,
      withSoftDelete,
      join,
      orderBy,
      groupBy,
      limit,
      ...getLimitOffset,
    });

    if (transaction) {
      return transaction.map(
        query,
        {
          ...this.buildQueryValue(where),
          limit,
          ...getLimitOffset,
        },
        transactionCallback
      );
    }

    // console.log({
    //   ...this.buildQueryValue(where),
    //   ...getLimitOffset,
    // });

    try {
      return await this.database.query(query, {
        ...this.buildQueryValue(where),
        limit,
        ...getLimitOffset,
      });
    } catch (e) {
      console.log(e);
    }
  }

  async baseGetOne(
    {
      columns = '*',
      where = {},
      join = [],
      withSoftDelete = false,
      orderBy,
      transaction,
      transctionCallback,
    } = {
      withSoftDelete: false,
    }
  ) {
    const query = this.buildSelectQuery({
      columns,
      where,
      withSoftDelete,
      join,
      orderBy,
    });

    if (transaction) {
      return transaction.oneOrNone(
        query,
        this.buildQueryValue(where),
        transctionCallback
      );
    }

    const [one] = await this.database.query(query, {
      ...this.buildQueryValue(where),
      limit: 1,
    });
    return one;
  }

  async baseUpdate(
    { where = {}, _set = {}, withSoftDelete = false, transaction } = {
      withSoftDelete: false,
    }
  ) {
    const set = this.filterNullFromSet(_set);
    const query = this.updateQueryBuilder({ where, _set: set, withSoftDelete });

    if (transaction) {
      return transaction.any(query, {
        ...this.buildQueryValue(where),
        ...set,
      });
    }
    const updated = await this.database.query(query, {
      ...this.buildQueryValue(where),
      ...set,
    });
    // console.log(updated);
    return updated;
  }

  async baseSoftDelete({
    where = null,
    _set = { ['deleted_at']: new Date().toISOString() },
  }) {
    if (!where) {
      throw new Error('Soft delete must have condition');
    }
    return await this.baseUpdate({ where, _set });
  }

  async baseDelete({
    where = null,
    _set = { deleted_at: new Date().toISOString },
  }) {
    if (!where) {
      throw new Error('Delete must have condition');
    }
    const query = this.deleteQueryBuilder({ where });
    console.log(query);
    const deleted = await this.database.query(
      query,
      this.buildQueryValue(where)
    );
    return deleted;
  }
  /**
   * INSERT
   * @param {*} data
   * @returns
   */
  buildInsertQuery(data) {
    const columns = Object.keys(data).filter(
      (key) => data[key] !== null && data[key] !== undefined
    );
    const query = `INSERT INTO ${this.tableName} (${this.getInertColumnNames(
      columns
    )}) VALUES (${this.getInertColumnValues(columns)}) RETURNING *`;
    console.log(query);
    return query;
  }

  getInertColumnNames(columns) {
    return columns.join(', ');
  }
  getInertColumnValues(columns) {
    return [...columns].map((column) => `$[${column}]`).join(', ');
  }
  /**
   * SELECT
   * @param {*} data
   * @returns
   */
  buildSelectQuery({
    columns,
    where,
    join,
    withSoftDelete,
    limit,
    offset,
    orderBy,
    groupBy,
  }) {
    console.log(
      `SELECT ${columns} FROM ${this.tableName} ${
        join.length > 0 ? this.joins(join) : ''
      } ${this.getConditions({
        where,
        withSoftDelete,
        limit,
        offset,
        orderBy,
        groupBy,
      })}`
    );
    return `SELECT ${columns} FROM ${this.tableName} ${
      join.length > 0 ? this.joins(join) : ''
    } ${this.getConditions({
      where,
      withSoftDelete,
      limit,
      offset,
      orderBy,
      groupBy,
    })}`;
  }

  buildQueryValue(where) {
    if (Object.keys(where).length === 0) {
      return {};
    }
    return this.getOneStepConditionQueryValue(where);
  }

  getWhere(where) {
    if (Object.keys(where).length > 0) {
      return this.getOneStepConditionQuery(where);
    } else {
      return '';
    }
  }

  getSoftDelete({ where, withSoftDelete }) {
    const conditions = Object.keys(where);
    let queryCondition = this.getWhere(where);
    if (conditions.length === 0 || queryCondition.trim().length === 0) {
      return ` ${
        !withSoftDelete ? `WHERE ${this.tableName}.deleted_at IS NULL ` : ''
      }`;
    }

    if (!withSoftDelete) {
      return ` WHERE (${this.getOneStepConditionQuery(where)}) AND ${
        this.tableName
      }.deleted_at IS NULL `;
    }

    return ` WHERE ${this.getOneStepConditionQuery(where)}`;
  }

  getConditions({ where, withSoftDelete, limit, offset, orderBy, groupBy }) {
    const queryCondition = `${this.getSoftDelete({
      where,
      withSoftDelete,
    })} ${this.groupBy(groupBy)} ${this.orderBy(orderBy)} ${this.offset(
      offset
    )} ${this.limit(limit)} `;

    return queryCondition;
  }

  getDuplicateColumn(compare) {
    if (compare.length === 0) {
      return compare;
    }
    let obj = {};
    for (let i = 0; i < compare.length; i++) {
      let selectedCompare = compare[i];
      let [[col, where]] = Object.entries(selectedCompare);
      if (obj[col]) {
        delete Object.assign(compare[i], {
          [`${col}-${obj[col]}`]: compare[i][col],
        })[col];
        obj[col]++;
      } else {
        obj[col] = 1;
      }
    }

    return compare;
  }

  getOneStepConditionQuery(obj) {
    let output = '';

    if (Object.keys(obj).length === 0) {
      return '';
    }
    let [val] = Object.values(obj);
    if (Object.keys(val).length === 0) {
      return '';
    }

    let [[col, compare]] = Object.entries(obj);

    if (Array.isArray(compare) && compare.length === 0) {
      return '';
    }

    console.log(val);
    let [[ass, value]] = Object.entries(compare);

    if (this.logicKey.includes(col)) {
      // this.getDuplicateColumn(compare);
      let tempQuery = this.getDuplicateColumn(compare)
        .map((c) => this.getOneStepConditionQuery(c))
        .filter((c) => c !== '')
        .join(` ${this.logicMap(col)} `);
      output = `${
        tempQuery.trim().length > 0 ? `${output} (${tempQuery})` : ''
      }`;
    } else if (this.conditionKeys.includes(ass)) {
      let [filterdCol, count] = col.split('-');
      if (value !== null && value !== undefined) {
        if (ass === '_null' || ass === '_nnull') {
          output = `${filterdCol} ${this.conditionsMap(ass)}`;
        } else {
          output = `${filterdCol} ${this.conditionsMap(
            ass
          )} $[${this.getJointColumn(filterdCol)}${count || ''}]`;
        }
      } else {
        return '';
      }
    }

    return output;
  }

  getJointColumn(col) {
    let [table, column] = col.split('.');
    if (column) {
      return column;
    }
    return table;
  }
  getOneStepConditionQueryValue(where) {
    let output = {};
    if (Object.keys(where).length === 0) {
      return {};
    }
    let [val] = Object.values(where);
    if (Object.keys(val).length === 0) {
      return '';
    }

    let [[col, compare]] = Object.entries(where);
    if (Array.isArray(compare) && compare.length === 0) {
      return {};
    }
    let [[ass, value]] = Object.entries(compare);
    if (this.logicKey.includes(col)) {
      let ob = this.getDuplicateColumn(compare).map((c) =>
        this.getOneStepConditionQueryValue(c)
      );
      ob.forEach((element) => {
        if (!Array.isArray(element)) {
          output = { ...output, ...element };
        }
      });
    } else if (this.conditionKeys.includes(ass)) {
      let [filterdCol, count] = col.split('-');
      output[`${this.getJointColumn(filterdCol)}${count || ''}`] = value;
    }

    return output;
  }

  /**
 * 
 * @param {*} tables 
 * [
    { parentTable: 'parentColumn', joiningTable: 'joiningColumn' },
    { coach: 'id', bag: 'bag_id' },
  ]
 * @returns 
 */
  joins(tables) {
    let output = [...tables]
      .map((item) => {
        let [[parentTable, parentColumn], [joiningTable, joiningColumn]] =
          Object.entries(item);
        return `${joiningTable} ON ${parentTable}.${parentColumn} = ${this.joinTableRename(
          joiningTable
        )}.${joiningColumn}`;
      })
      .join(' LEFT JOIN ');
    return ` LEFT JOIN ${output}`;
  }
  joinTableRename(joiningTable) {
    let [tableName, tableNewName] = joiningTable.split(' as ');
    if (tableNewName) {
      return tableNewName.trim();
    }
    return tableName;
  }

  limit(limit) {
    if (limit) {
      return ' LIMIT $[limit]';
    }
    return '';
  }

  offset(offset) {
    if (offset >= 0) {
      return ' OFFSET $[offset]';
    }
    return '';
  }
  groupBy(groupBy) {
    if (groupBy) {
      return `GROUP BY ${groupBy}`;
    }
    return '';
  }

  orderBy(orderBy) {
    if (!orderBy) {
      return '';
    }

    const order = Object.entries(orderBy)
      .map(([column, order]) => `${column} ${order} `)
      .join(',');

    return ` ORDER BY ${order}`;
  }

  getOffsetValue({ page }) {
    if (page) {
      const { limit, offset } = this.helper.getLimitOffset({ page });

      return { limit, offset };
    }
    return {};
  }

  updateQueryBuilder({ where, _set, withSoftDelete }) {
    const query = `UPDATE ${this.tableName} SET ${this.updateSetQueryBuilder({
      _set,
    })} ${this.getConditions({ where, withSoftDelete })} RETURNING *`;
    return query;
  }

  updateSetQueryBuilder({ _set }) {
    return Object.entries(_set)
      .map(([column, value]) => {
        return `${column} = $[${column}]`;
      })
      .join(', ');
  }

  filterNullFromSet(set) {
    let _set = {};
    Object.keys(set).forEach((key) => {
      if (set[key] !== null && set[key] !== undefined) {
        _set[key] = set[key];
      }
    });
    _set['updated_at'] = new Date().toISOString();
    return _set;
  }

  /**
   *
   * @param {*} tables
   * DELETE
   * @returns
   */

  deleteQueryBuilder({ where, withSoftDelete }) {
    return `DELETE FROM ${this.tableName} ${this.getConditions({
      where,
      withSoftDelete: true,
    })}`;
  }

  conditionsMap(condition) {
    switch (condition) {
      case '_eq':
        return '=';
      case '_neq':
        return '!=';
      case '_gt':
        return '>';
      case '_gte':
        return '>=';
      case '_lt':
        return '<';
      case '_lte':
        return '<=';
      case '_nnull':
        return 'IS NOT NULL';
      case '_null':
        return 'IS NULL';
      case '_like':
        return 'LIKE';
      case '_ilike':
        return 'ILIKE';
    }
  }

  logicMap(logic) {
    switch (logic) {
      case '_and':
        return 'AND';
      case '_or':
        return 'OR';
    }
  }
}

module.exports = BaseRepository;
