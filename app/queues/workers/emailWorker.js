const { publishToRabitmq } = require('../publishers');

class EmailWorker {
  static async send(payload) {
    try {
      await publishToRabitmq({
        worker: 'email_sending_queue',
        queue: 'email_sending_queue',
        message: {
          action: 'send',
          type: 'email',
          data: payload,
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = EmailWorker;
