const { publishToRabitmq } = require('../../publishers');

class BulkUpdateLassraNumberWorker {
  static async send({ bucket_url, profile_id }) {
    try {
      await publishToRabitmq({
        worker: 'update_student_queue',
        message: {
          action: 'update',
          type: 'lassra_number',
          data: { bucket_url, profile_id },
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = BulkUpdateLassraNumberWorker;
