const { publishToRabitmq } = require('../../publishers');

class BulkUpdateLassraNumberWorker {
  static async send({ bucket_url, cut_off }) {
    try {
      await publishToRabitmq({
        worker: 'update_student_queue',
        message: {
          action: 'update',
          type: 'applicant_score',
          data: { bucket_url, cut_off },
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = BulkUpdateLassraNumberWorker;
