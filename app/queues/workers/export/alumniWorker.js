const { publishToRabitmq } = require('../../publishers');

class ExportAlumniWorker {
  static async send({
    profile_id,
    program_type_id,
    employment_status,
    qualification_id,
    gender,
    from_date,
    to_date,
    sector_id,
    course_id,
    s,
  }) {
    try {
      await publishToRabitmq({
        worker: 'export_queue',
        message: {
          action: 'export',
          type: 'alumni',
          data: {
            program_type_id,
            employment_status,
            qualification_id,
            gender,
            from_date,
            to_date,
            sector_id,
            course_id,
            profile_id,
            s,
          },
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = ExportAlumniWorker;
