const { publishToRabitmq } = require('../../publishers');

class ExportStudentWorker {
  static async send({
    profile_id,
    program_type_id,
    status,
    qualification_id,
    gender,
    from_date,
    to_date,
    sector_id,
    course_id,
    s,
  }) {
    try {
      await publishToRabitmq({
        worker: 'export_queue',
        message: {
          action: 'export',
          type: 'student',
          data: {
            program_type_id,
            status,
            qualification_id,
            gender,
            from_date,
            to_date,
            sector_id,
            course_id,
            profile_id,
            s,
          },
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = ExportStudentWorker;
