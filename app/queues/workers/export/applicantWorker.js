const { publishToRabitmq } = require('../../publishers');

class ExportApplicationWorker {
  static async send({
    profile_id,
    program_type_id,
    status,
    qualification_id,
    gender,
    from_date,
    to_date,
    sector_id,
    course_id,
    s,
    where,
  }) {
    try {
      await publishToRabitmq({
        worker: 'export_queue',
        message: {
          action: 'export',
          type: 'applicant',
          data: {
            program_type_id,
            status,
            qualification_id,
            gender,
            from_date,
            to_date,
            sector_id,
            course_id,
            profile_id,
            s,
            where,
          },
        },
      });
    } catch (e) {
      console.error(e);
    }
  }
}

module.exports = ExportApplicationWorker;
