const { Parser } = require('json2csv');
const fs = require('fs-extra');
const defaultFs = require('fs');
const Helper = require('./../../utils/Helper');

const toCSV = ({ fields, data }) => {
  const parser = new Parser({ fields });
  return parser.parse(data);
};

const writeToFile = ({ content, fileName }) => {
  const path = `${__dirname}/../../../tmp/export/${fileName}`;
  fs.outputFileSync(path, content);
  return path;
};

const deleteFile = (path) => {
  defaultFs.unlinkSync(path);
};

module.exports = { toCSV, writeToFile, deleteFile };
