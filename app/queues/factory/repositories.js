const config = require('./../config');
const { helper } = config;
const ApplicationRepository = require('../../repositories/ApplicantRepository');
const ProfileRepository = require('../../repositories/ProfileRepository');
const RoleRepository = require('../../repositories/RoleRepository');
const UsersModelRepository = require('../../repositories/UsersModelRepository');
const UsersRepository = require('../../repositories/UsersRepository');
const ProfileCourseRepository = require('../../repositories/ProfileCourseRepository');

const NotificationService = require('./../../services/NotificationService');

const applicantRepository = new ApplicationRepository({
  ...config,
});
const profileRepository = new ProfileRepository({
  ...config,
});
const roleRepository = new RoleRepository({
  ...config,
});

const usersModelRepository = new UsersModelRepository({
  ...config,
});

const usersRepository = new UsersRepository({
  ...config,
});

const notificationService = new NotificationService({
  ...config,
});

const profileCourseRepository = new ProfileCourseRepository({ ...config });

module.exports = {
  applicantRepository,
  profileRepository,
  roleRepository,
  usersModelRepository,
  usersRepository,
  helper,
  notificationService,
  profileCourseRepository,
};
