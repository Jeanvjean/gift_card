const {
  applicantRepository,
  profileRepository,
  notificationService,
} = require('./repositories');

const csv = require('csv2');
const map = require('through2-map');
const {
  APPROVE_APPLICANT_STATUS,
  DECLINE_APPLICANT_STATUS,
} = require('../../constants');

export const updateScore = async (row, cut_off) => {
  const [sn, last_name, first_name, middle_name, email, score] = row;
  console.log(email, score)
  if (String(score.length) >= 1 && parseFloat(score) >= 0) {
    const { vtc_screen_status, name, program_name } =
      await applicantRepository.baseGetOne({
        columns: `program_type.slug as program_name, applicant.screening_count, applicant.vtc_screen_status,
         applicant.screening_partner_screen_status, applicant.lassra_number, applicant.email, 
         CONCAT(applicant.last_name, ' ',  applicant.first_name) as name`,
        where: { email: { _eq: email } },
        join: [{ applicant: 'program_type_id', program_type: 'id' }],
      });
    let status = DECLINE_APPLICANT_STATUS;
    let screening_partner_screen_status = parseFloat(cut_off) <= parseFloat(score) ? APPROVE_APPLICANT_STATUS : DECLINE_APPLICANT_STATUS;

    if (
      vtc_screen_status === APPROVE_APPLICANT_STATUS &&
      screening_partner_screen_status === APPROVE_APPLICANT_STATUS
    ) {
      status = APPROVE_APPLICANT_STATUS;
    }
    await applicantRepository.baseUpdate({
      where: { email: { _eq: email } },
      _set: {
        screening_partner_screen_status,
        score,
        cutoff_score: cut_off,
        status,
      },
    });
    /**decline email */
    if (status === DECLINE_APPLICANT_STATUS) {
      await notificationService.declineApplicationScreened({
        email,
        name,
        program_name,
      });
    }
  }
};


module.exports = async ({ bucket_url, cut_off }) => {
  const extract = map({ objectMode: true }, async (row) => {
    // console.log(row)
    if (/^[0-9]*$/.test(row[0])) {
      await updateScore(row, cut_off);
    }
  });
  return new Promise((resolve, reject) => {
    const https = require('https');
    https.get(bucket_url, (stream) => {
      stream
        .pipe(csv())
        .pipe(extract)
        .on('finish', () => {
          // sendProcessedEmail({ email, name });
          resolve({ status: true });
        });
    });
  });
};
