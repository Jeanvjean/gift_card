const {
  applicantRepository,
  profileRepository,
  notificationService,
} = require('./repositories');

const csv = require('csv2');
const map = require('through2-map');

const sendProcessedEmail = ({ email, name }) => {
  notificationService.sendEmailToQueue({
    data: { name },
    email,
    title: 'Application Update Processed',
    subject: 'Application Update Processed',
    templateFileName: 'processedApplicationUpdate.ejs',
  });
};

const updateLassraNumber = async (row) => {
  const [sn, id, name, lassra_number] = row;

  if (String(lassra_number.length) > 1) {
    await applicantRepository.baseUpdate({
      where: { id: { _eq: id } },
      _set: { lassra_number, training_status: 'in_training' },
    });
  }
};

module.exports = async ({ bucket_url, profile_id }) => {
  const { first_name, last_name, email } = await profileRepository.baseGetOne({
    columns: 'first_name, last_name, email',
    where: { id: { _eq: profile_id } },
  });

  const name = `${first_name} ${last_name}`;

  const extract = map({ objectMode: true }, async (row) => {
    if (/^[0-9]*$/.test(row[0])) {
      await updateLassraNumber(row);
    }
  });

  return new Promise((resolve, reject) => {
    const https = require('https');
    https.get(bucket_url, (stream) => {
      stream
        .pipe(csv())
        .pipe(extract)
        .on('finish', () => {
          sendProcessedEmail({ email, name });
          resolve({ status: true });
        });
    });
  });
};
