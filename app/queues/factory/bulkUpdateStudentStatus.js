const {
  applicantRepository,
  usersModelRepository,
  roleRepository,
  usersRepository,
  notificationService,
  profileRepository,
  helper,
} = require('./repositories');
const fs = require('fs'),
  csv = require('csv2'),
  map = require('through2-map');

const sendProcessedEmail = ({ email, name }) => {
  notificationService.sendEmailToQueue({
    data: { name },
    email,
    title: 'Student Update Processed',
    subject: 'Student Update Processed',
    templateFileName: 'processedStudentUpdate.ejs',
  });
};

const certifyStudent = async (row) => {
  const [sn, id, name, lassra_number, status] = row;
  return await applicantRepository.baseUpdate({
    where: { id: { _eq: id } },
    _set: { status, training_status: status },
  });
};
module.exports = async ({ bucket_url, profile_id }) => {
  const { first_name, last_name, email } = await profileRepository.baseGetOne({
    columns: 'first_name, last_name, email',
    where: { id: { _eq: profile_id } },
  });

  const name = `${first_name} ${last_name}`;

  const extract = map({ objectMode: true }, async (row) => {
    if (/^[0-9]*$/.test(row[0])) {
      const [sn, id, name, lassra_number, status] = row;

      if (
        String(id.length) > 1 &&
        status === 'certified' /*|| status === 'not_certified'*/
      ) {
        const certified = await certifyStudent(row);
        if (certified.length > 0) {
          const {
            first_name,
            last_name,
            email,
            lassra_number,
            id: applicant_id,
          } = await applicantRepository.baseGetOne({
            where: { id: { _eq: id } },
          });

          await signupAlumni({
            email,
            first_name,
            last_name,
            lassra_number,
            applicant_id,
          });
        }
      }
    }
  });

  return new Promise((resolve, reject) => {
    const https = require('https');
    https.get(bucket_url, (stream) => {
      stream
        .pipe(csv())
        .pipe(extract)
        .on('finish', () => {
          sendProcessedEmail({ email, name });
          resolve({ status: true });
        });
    });
  });
};

const signupAlumni = async ({
  email,
  first_name,
  last_name,
  lassra_number,
  applicant_id,
}) => {
  const user_type = 'alumni';

  const password = helper.generatePassword();
  const hashPassword = await helper.generatePasswordHash({ password });

  const { id: user_id } = await usersRepository.baseInsert({
    data: {
      email,
      password: hashPassword,
      lassra_number,
    },
  });

  const { id: role_id } = await roleRepository.baseGetOne({
    columns: 'id',
    where: { slug: { _eq: user_type } },
  });

  const userModel = await usersModelRepository.baseInsert({
    data: {
      applicant_id,
      user_id,
      role_id,
    },
  });

  await notificationService.sendSignupMail({
    password,
    email,
    name: `${last_name} ${first_name}`,
    lassra_number,
    user_type,
  });
  return true;
};
