const {
  applicantRepository,
  profileCourseRepository,
  profileRepository,
} = require('./repositories');
const { toCSV, writeToFile, deleteFile } = require('../utils/helper');
const {
  sendEmail,
  base64EncodeFile,
  fileGetContent,
} = require('../../utils/mail');
module.exports = async ({
  program_type_id,
  employment_status,
  qualification_id,
  gender,
  from_date,
  to_date,
  sector_id,
  course_id,
  profile_id,
  s,
}) => {
  const { user_type, email, first_name, last_name } =
    await profileRepository.baseGetOne({
      where: { id: { _eq: profile_id } },
    });
  const userType = await getUserType({ profile_id, user_type });

  const applicants = await applicantRepository.baseGetAll({
    columns: `applicant.id as id, applicant.unique_id as unique_id, applicant.first_name as first_name, applicant.last_name as last_name, 
        applicant.phone_number as phone_number, applicant.lassra_number as lassra_number, program_type.name as program, 
        sector.name as sector, course.name as course, applicant.status as status,
        vtc_profile.company_name as vtc, program_type.id as program_type_id, applicant.address as address, applicant.created_at as applied_on`,
    where: {
      _and: [
        { 'applicant.program_type_id': { _eq: program_type_id } },
        { 'applicant.training_status': { _eq: 'certified' } },
        { 'applicant.employment_status': { _eq: employment_status } },
        { 'applicant.qualification_id': { _eq: qualification_id } },
        { 'applicant.gender': { _eq: gender } },
        { 'sector.id': { _eq: sector_id } },
        { 'course.id': { _eq: course_id } },
        {
          _and: [
            { 'applicant.created_at': { _gte: from_date } },
            { 'applicant.created_at': { _lte: to_date } },
          ],
        },
        userType,
        {
          _or: [
            { 'applicant.first_name': { _ilike: s ? `%${s}%` : null } },
            { 'applicant.last_name': { _ilike: s ? `%${s}%` : null } },
            { 'applicant.middle_name': { _ilike: s ? `%${s}%` : null } },
            { 'applicant.unique_id': { _ilike: s ? `%${s}%` : null } },
            { 'applicant.phone_number': { _ilike: s ? `%${s}%` : null } },
            { 'applicant.email': { _ilike: s ? `%${s}%` : null } },
          ],
        },
      ],
    },

    join: [
      { applicant: 'program_type_id', program_type: 'id' },
      { applicant: 'sector_id', sector: 'id' },
      {
        applicant: 'course_id',
        course: 'id',
      },
      {
        applicant: 'vtc_screen_profile_id',
        'profile as vtc_profile': 'id',
      },
    ],
    orderBy: { 'applicant.first_name': 'ASC' },
  });

  const data = applicants.map(
    (
      {
        first_name,
        last_name,
        middle_name,
        lassra_number,
        program,
        sector,
        course,
        status,
        vtc,
        address,
        applied_on,
      },
      index
    ) => ({
      'S/N': index + 1,
      Name: `${last_name} ${first_name} ${middle_name ? middle_name : ''}`,
      'Lassra Number': lassra_number,
      Program: program,
      Sector: sector,
      Course: course,
      Status: status,
      Vtc: vtc,
      Address: address,
      'Applied on': applied_on,
    })
  );

  const content = toCSV({
    fields: [
      'S/N',
      'Name',
      'Lassra Number',
      'Program',
      'Sector',
      'Course',
      'Status',
      'Vtc',
      'Address',
      'Applied on',
    ],
    data,
  });
  console.log(content);

  const fileName = `${new Date().getTime()}.csv`;
  const filePath = writeToFile({ fileName, content });
  const base64Content = await base64EncodeFile(filePath);

  await sendEmail({
    subject: 'subject',
    // bcc,
    // cc,
    title: 'Data Download Request',
    filePath: 'exportCsv.ejs',
    payload: {
      name: `${first_name} ${last_name}`,
      file: fileGetContent(filePath),
      title: 'Data Download Request',
    },
    from: 'israel.hmis@gmail.com',
    to: email,
    attachment: true,
    fileName: 'lsesp_data.csv',
    fileType: 'text/csv',
    content: base64Content,
  });
  deleteFile(filePath);
};

const getUserType = async ({ profile_id, user_type }) => {
  const { user_type: userType } = await profileRepository.baseGetOne({
    where: { id: { _eq: profile_id } },
  });

  if (userType === 'vtc') {
    const courses = await profileCourseRepository.baseGetAll({
      where: { profile_id: { _eq: profile_id } },
    });
    return buildVtcCourseQuery(courses);
  }
  if (userType === 'screeningPartner') {
    return {
      'applicant.screening_partner_profile_id': {
        _eq: profile_id,
      },
    };
  }
  return {};
};

const buildVtcCourseQuery = (courses) => {
  return {
    _or: courses.map(({ course_id }) => {
      return { 'applicant.course_id': { _eq: course_id } };
    }),
  };
};
