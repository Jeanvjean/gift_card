const {
  applicantRepository,
  profileCourseRepository,
  profileRepository,
} = require('./repositories');
const { toCSV, writeToFile, deleteFile } = require('../utils/helper');
const {
  sendEmail,
  base64EncodeFile,
  getFileSize,
  fileGetContent,
} = require('../../utils/mail');
module.exports = async ({
  program_type_id,
  status,
  qualification_id,
  gender,
  from_date,
  to_date,
  sector_id,
  course_id,
  profile_id,
  s,
  where,
}) => {
  const { user_type, email, first_name, last_name } =
    await profileRepository.baseGetOne({
      where: { id: { _eq: profile_id } },
    });
  console.log(JSON.stringify(where));
  // const userType = await getUserType({ profile_id, user_type });

  const applicants = await applicantRepository.baseGetAll({
    columns: `applicant.id as id, applicant.unique_id as unique_id, applicant.first_name as first_name, applicant.last_name as last_name, 
    applicant.phone_number as phone_number, applicant.gender as gender, applicant.email as email, local_government_area.name as local_government_area,
    sector.name as sector, applicant.status as status, vtc.company_name as vtc, program.name as program, course.name as course, applicant.created_at as applied_on,
    applicant.address as location
    `,
    where,
    join:[
      {
        applicant: 'local_government_area_id',
        local_government_area: 'id',
      },
      {
        applicant: 'sector_id',
        sector: 'id',
      },
      {
        applicant: 'vtc_screen_profile_id',
        "profile as vtc": 'id',
      },
      {
        applicant: 'program_type_id',
        "program_type as program": 'id',
      },
      {
        applicant: 'course_id',
        course: 'id',
      },
    ]
  });

  const data = app_data.map(
    (
      {
        first_name, 
        last_name, 
        middle_name, 
        unique_id, 
        phone_number,
        location, 
        gender, 
        email, 
        local_government_area, 
        sector,
        vtc, 
        program, 
        course, 
        applied_on 
      }, index)=> ({
    'S/N': index + 1,
    'Name': `${last_name} ${first_name} ${middle_name ? middle_name : ''}`,
    'Lassra Number': unique_id,
    'Phone Number': phone_number,
    'Location':location,
    'Gender': gender,
    'Email': email,
    'Local Government':local_government_area,
    'Sector': sector,
    'Vtc': vtc,
    'Program': program,
    'Course': course,
    'Applied On':applied_on
}))

  const content = toCSV({
    fields: [
      'S/N',
      'Name', 
      'Lassra Number', 
      'Phone Number', 
      'Location', 
      'Gender', 
      'Email', 
      'Local government', 
      'Sector', 
      'Program', 
      'Vtc', 
      'Course', 
      'Applied on'
    ],
    data,
  });
 console.log(content);
  const fileName = `${new Date().getTime()}.csv`;
  const filePath = writeToFile({ fileName, content });
  const base64Content = await base64EncodeFile(filePath);

  await sendEmail({
    subject: 'subject',
    // bcc,
    cc:'fjohn087@gmail.com',
    title: 'Data Download Request',
    filePath: 'exportCsv.ejs',
    payload: {
      name: `${first_name} ${last_name}`,
      file: fileGetContent(filePath),
      title: 'Data Download Request',
    },
    from: 'israel.hmis@gmail.com',
    to: email,
    attachment: true,
    fileName: 'lsesp_data.csv',
    fileType: 'text/csv',
    content: base64Content,
  });
  deleteFile(filePath);
};

const getUserType = async ({ profile_id, user_type }) => {
  const { user_type: userType } = await profileRepository.baseGetOne({
    where: { id: { _eq: profile_id } },
  });

  if (userType === 'vtc') {
    const courses = await profileCourseRepository.baseGetAll({
      where: { profile_id: { _eq: profile_id } },
    });
    return buildVtcCourseQuery(courses);
  }
  if (userType === 'screeningPartner') {
    return {
      'applicant.screening_partner_profile_id': {
        _eq: profile_id,
      },
    };
  }
  return {};
};

const buildVtcCourseQuery = (courses) => {
  return {
    _or: courses.map(({ course_id }) => {
      return { 'applicant.course_id': { _eq: course_id } };
    }),
  };
};
