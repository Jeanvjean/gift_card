const path = require('path');

require('dotenv').config({
  path: path.resolve(`${__dirname}/../../../.env`),
});

// const { getDatabase } = require('../../utils/rabbitmqDb');

const config = require('../../../config');
const providers = require('../../../providers');
const database = providers.database.boot({ config });

const Helper = require('../../utils/Helper');
const helper = new Helper({
  database,
  config,
});

module.exports = { config, providers, database, helper };
