require('./../config');

const {
  rabbitmqArchitecture,
  rabbitmq,
  saveFailedJobs,
} = require('../../utils/rabbitmq');
const exportAlumni = require('../factory/exportAlumni');

// const exportApplication = require('./../factory/exportApplication');
const exportApplicant = require('./../factory/exportApplicant');
const exportStudent = require('./../factory/exportStudent');

let retry = 0;

const run = async () => {
  const { channel } = await rabbitmq();
  const { queue, exchange, routingKey } = await rabbitmqArchitecture(
    'export_queue'
  );

  console.log(queue);

  // create the exchange if it doesn't already exist
  await channel.assertExchange(exchange, 'topic', { durable: true });
  console.log(exchange);
  // create the queue if it doesn't already exist
  const q = await channel.assertQueue(queue, { durable: true });
  // bind queue to exchange
  await channel.bindQueue(q.queue, exchange, routingKey);
  console.log(' [*] Waiting for %s. To exit press CTRL+C', queue);
  // get one message off the queue at a time
  await channel.prefetch(1);

  try {
    // consume message from queue
    await channel.consume(
      q.queue,
      async (msg) => {
        const message = JSON.parse(msg.content.toString());
        const { action, type } = message;
        // console.log(message);
        try {
          switch (`${action}_${type}`) {
            case 'export_applicant':
              {
                const {
                  data: {
                    program_type_id,
                    status,
                    qualification_id,
                    gender,
                    from_date,
                    to_date,
                    sector_id,
                    course_id,
                    profile_id,
                    s,
                    vtc,
                    where,
                  },
                } = message;
                console.log(' [Received] %s', message.action);
                await exportApplicant({
                  program_type_id,
                  status,
                  qualification_id,
                  gender,
                  from_date,
                  to_date,
                  sector_id,
                  course_id,
                  profile_id,
                  s,
                  vtc,
                  where,
                });

                console.log(
                  ' [Processed] %s',
                  `Sending mail - ${message.action}`
                );
                channel.ack(msg); // acknowledged processing is complete
              }
              break;

            case 'export_student':
              {
                const {
                  data: {
                    program_type_id,
                    status,
                    qualification_id,
                    gender,
                    from_date,
                    to_date,
                    sector_id,
                    course_id,
                    profile_id,
                    s,
                    vtc,
                  },
                } = message;
                console.log(' [Received] %s', message.action);
                await exportStudent({
                  program_type_id,
                  status,
                  qualification_id,
                  gender,
                  from_date,
                  to_date,
                  sector_id,
                  course_id,
                  profile_id,
                  s,
                  vtc,
                });

                console.log(
                  ' [Processed] %s',
                  `Sending mail - ${message.action}`
                );
                channel.ack(msg); // acknowledged processing is complete
              }
              break;
            case 'export_alumni':
              {
                const {
                  data: {
                    program_type_id,
                    employment_status,
                    qualification_id,
                    gender,
                    from_date,
                    to_date,
                    sector_id,
                    course_id,
                    profile_id,
                    s,
                    vtc,
                  },
                } = message;
                console.log(' [Received] %s', message.action);
                await exportAlumni({
                  program_type_id,
                  employment_status,
                  qualification_id,
                  gender,
                  from_date,
                  to_date,
                  sector_id,
                  course_id,
                  profile_id,
                  s,
                  vtc,
                });

                console.log(
                  ' [Processed] %s',
                  `Sending mail - ${message.action}`
                );
                channel.ack(msg); // acknowledged processing is complete
              }
              break;
            default:
              break;
          }
        } catch (error) {
          console.error({ UploadApplicantProcessed: error });
          retry = parseInt(retry + 1);
          if (retry >= 3) {
            // insert job into failed jobs
            await saveFailedJobs({
              worker_name: 'upload_applicant_queue',
              message_action: message.action,
              message_type: message.type,
              message_data: message.data,
              error_message: error.message,
            });
            // console.error({ SuccessfullySavedIntoDB: error });
            // channel.ack(msg);
          } else {
            // channel.nack(msg);
          }
        }
      },
      { noAck: false } // ensure that message acknowledged after processed - it must be false to work like so
    );
  } catch (err) {
    console.log({ err });
  }
};
// call function
run();
