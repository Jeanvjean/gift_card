import expressJoi from 'express-joi-validation';
import AppRoute from './app';
import AuthRoute from './auth';
import GiftRoute from './gift';
import PaymentRoute from './payment';
import transactionRoute from './transaction';
import giftCardRoute from './giftCard';

import checkErrors from '../middlewares/check_errors';
import { MethodNotAllowed } from '../tools/errors';

const { makeInvoker } = require('awilix-express');

const express = require('express');

const Validator = expressJoi.createValidator({
  passError: true,
});

module.exports = () => {
  const router = express.Router();

  const routers = [].concat([
    AppRoute,
    AuthRoute,
    GiftRoute,
    PaymentRoute,
    transactionRoute,
    giftCardRoute
  ]);

  for (let i = 0; i <= routers.length - 1; i++) {
    routers[i](router, Validator, checkErrors, makeInvoker, MethodNotAllowed);
  }
  // enable app routes

  return router;
};
