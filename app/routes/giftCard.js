import GiftCardController from '../controllers/GiftCardContoller';
import auth from '../middlewares/auth';

module.exports = (
    router,
    Validator,
    check_errors,
    makeInvoker,
    MethodNotAllowed
) =>{
    const controller = makeInvoker(GiftCardController);

    router.get(
        '/my-giftcards',
        auth,
        check_errors(controller('fetchGiftCards'))
    )
    .all(MethodNotAllowed)

    router.post(
        '/redeem-card',
        check_errors(controller('redeemGiftCard'))
    )
    .all(MethodNotAllowed);

    return router;
}