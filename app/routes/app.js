const AppController = require('../controllers/AppController');
const {
  somSchema,
  /*importerGenerator*/
} = require('./../validations/app');

module.exports = (
  router,
  Validator,
  check_errors,
  makeInvoker,
  MethodNotAllowed
) => {
  const controller = makeInvoker(AppController);

	router
    .route('/')
    .get(check_errors(controller('index')))
    .all(MethodNotAllowed);
	router
    .get('/companyType', check_errors(controller('companyType')))
    .all(MethodNotAllowed);

	/*generator*/

	return router;
}
