const GiftCardTypesController = require('../controllers/GiftCardTypesController');
const { admin, superAdmin } = require('../middlewares/roleAccess');
const auth = require('../middlewares/auth');
const {
    createCardTypeSchema,
    updateCardTypeSchema
} = require('../validations/gift');

module.exports = (
    router,
    Validator,
    check_errors,
    makeInvoker,
    MethodNotAllowed
)=>{
    const controller = makeInvoker(GiftCardTypesController);

    router.post(
        '/create-card',
        auth,
        admin,
        Validator.body(createCardTypeSchema),
        check_errors(controller('createGiftCard'))
    )
    .all(MethodNotAllowed)

    router.get(
        '/fetch-gift-cards',
        auth,
        check_errors(controller('fetchGiftCards'))
    )
    .all(MethodNotAllowed)

    router.get(
        '/get-card/:card_id',
        auth,
        check_errors(controller('cardDetails'))
    )
    .all(MethodNotAllowed)

    router.delete(
        '/delete_card/:card_id',
        auth,
        admin,
        check_errors(controller('deleteCard'))
    )
    .all(MethodNotAllowed)

    router.put(
        '/update-card/:card_id',
        auth,
        admin,
        Validator.body(updateCardTypeSchema),
        check_errors(controller('updateCard'))
    )
    .all(MethodNotAllowed)

    return router;
}