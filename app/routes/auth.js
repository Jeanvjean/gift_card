const AuthController = require('../controllers/AuthController');
const { admin, superAdmin } = require('../middlewares/roleAccess');
// const { applicantUpload } = require('../utils/multer');
const {
  createForgetPasswordSchema,
  createLoginSchema,
  createResetPasswordSchema,
  createChangePasswordSchema,
  createSignupSchema,
  adminSignupSchema,
  /*importerGenerator*/
} = require('../validations/auth');
const auth = require('../middlewares/auth');

module.exports = (
  router,
  Validator,
  check_errors,
  makeInvoker,
  MethodNotAllowed
) => {
  const controller = makeInvoker(AuthController);

	router
    .post(
      '/signup',
      Validator.body(createSignupSchema),
      check_errors(controller('signup'))
    )
    .all(MethodNotAllowed);

    router.post(
      '/superadmin-signup',
      Validator.body(adminSignupSchema),
      check_errors(controller('superadminSignup'))
    )
    .all(MethodNotAllowed)

    router.post(
      '/create-admin',
      Validator.body(adminSignupSchema),
      auth,
      superAdmin,
      check_errors(controller('adminSignup'))
    )
    .all(MethodNotAllowed)

	router
    .post(
      '/login',
      Validator.body(createLoginSchema),
      check_errors(controller('login'))
    )
    .all(MethodNotAllowed);

	router
    .post(
      '/forget-password',
      Validator.body(createForgetPasswordSchema),
      check_errors(controller('forgetPassword'))
    )
    .all(MethodNotAllowed);

	router
    .post(
      '/reset-password',
      Validator.body(createResetPasswordSchema),
      check_errors(controller('resetPassword'))
    )
    .all(MethodNotAllowed);

	router
    .post(
      '/change-password',
      auth,
      Validator.body(createChangePasswordSchema),
      check_errors(controller('changePassword'))
    )
    .all(MethodNotAllowed);

	/*generator*/

	return router;
}
