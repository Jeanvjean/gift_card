const TransactionController = require("../controllers/TransactionController")
const auth = require('../middlewares/auth');

module.exports = (
    router,
    Validator,
    check_errors,
    makeInvoker,
    MethodNotAllowed
)=>{
    const controller = makeInvoker(TransactionController);

    router.post(
        '/delete-transactions',
        auth,
        check_errors(controller('deleteTransactions'))
    )
    .all(MethodNotAllowed)

    router.get(
        '/fetch-transactions',
        auth,
        check_errors(controller('fetchTransactions'))
    )
    .all(MethodNotAllowed);

    router.get(
        '/transaction/:transaction_id',
        auth,
        check_errors(controller('transactionDetails'))
    )
    .all(MethodNotAllowed)

    return router;
}