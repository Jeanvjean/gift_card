const PaymentController = require('../controllers/PaymentController');
const auth = require('../middlewares/auth');

module.exports = (
    router,
    Validator,
    check_errors,
    makeInvoker,
    MethodNotAllowed
)=>{
    const controller = makeInvoker(PaymentController);

    router.post(
        '/initiate-paystack-card',
        auth,
        check_errors(controller('initialiatePaystackCard'))
    )
    .all(MethodNotAllowed);

    router.post(
        '/charge-paystack-card',
        auth,
        check_errors(controller('paystackChargeCard'))
    )
    .all(MethodNotAllowed);

    router.get(
        '/init-stripe-card',
        auth,
        check_errors(controller('initStripeCard'))
    )
    .all(MethodNotAllowed);

    router.post(
        '/complete-stripe-init',
        auth,
        check_errors(controller('stripeCompleteInitCard'))
    )
    .all(MethodNotAllowed);

    router.post(
        '/charge-stripe-card',
        auth,
        check_errors(controller('stripeChargeCard'))
    )
    .all(MethodNotAllowed)

    router.post(
        '/delete-cards',
        auth,
        check_errors(controller('deleteCard'))
    )
    .all(MethodNotAllowed);

    router.get(
        '/fetch-banks',
        check_errors(controller('fetchBanks'))
    )
    .all(MethodNotAllowed);

    router.post(
        '/buy-giftcard',
        check_errors(controller('buyGiftyCard'))
    )
    .all(MethodNotAllowed);

    return router;
}