const path = require('path');
const ejs = require('ejs');
const ClientHttp = require('../utils/Client');
const EmailWorker = require('../queues/workers/emailWorker');
import pug from 'pug';
import juice from 'juice';
// import htmlToText from 'html-to-text';
// const mailjet = require('node-mailjet').connect(
// 	process.env.SMTP_PUBLIC,
// 	process.env.SMTP_PRIVATE
// );
// import ejs from 'ejs'

// const qs = require('qs');

class NotificationService {
  constructor({ errors, config, africastalkingService }) {
    this.config = config;

    // this.smsClient = new ClientHttp(this.baseUrl, this.headers);
    // this.smsClient = africasTalkingService;
    this.smsClient = africastalkingService;

    const emailHeaders = {
      'secret-key': config.get('notification.api_key'),
    };
    this.emailClient = new ClientHttp(
      config.get('notification.base_uri'),
      emailHeaders
    );
  }

  async queueEmail({
    email,
    filePath,
    subject,
    title,
    data,
    attachment,
    fileName,
    fileType,
    content,
  }) {
    await EmailWorker.send({
      title,
      to: email,
      subject,
      payload: { ...data, title },
      filePath,
      attachment,
      fileName,
      fileType,
      content,
    });
  }

  async sendEmail({ data, templatePath, subject, email, title }) {
    try {
      // const emailTemplate = await ejs.renderFile(
      //   `${__dirname}/../../views/emails/${templatePath}.ejs`,
      //   { ...data }
      // );
      const emailTemplate = await ejs.renderFile(
        path.join(__dirname, `../../views/emails/${templatePath}.ejs`),
        { ...data }
      );
      const message = {
        to: email,
        // bcc: ['akudevops2@gmail.com'],
        from: title
          ? `${title} <${this.config.get('notification.email.from_mail')}>`
          : this.config.get('notification.email.from_mail'),
        subject: subject,
        html: emailTemplate,
      };
      // console.log(message);
      const response = await this.emailClient.post(
        'notifications/email',
        message
      );
      return response;
    } catch (error) {
      console.log(error);
      throw new Error(error.message);
    }
  }

  async sendSms(payload) {
    try {
      const response = await this.smsClient.sendSms(payload);
      return response;
    } catch (error) {
      console.log('SEND SMS ERROR', error);
      throw new Error(error);
    }
  }

  async parsePhoneNumberToStandard(phoneNumbers) {
    try {
      let results = [];
      for (let i = 0; i <= phoneNumbers.length - 1; i++) {
        if (phoneNumbers[i].length === 11) {
          phoneNumbers[i] = `+234${phoneNumbers[i].substring(1)}`;
        }
        if (
          phoneNumbers[i].length === 13 &&
          phoneNumbers[i].substring(0, 1) !== '+'
        ) {
          phoneNumbers[i] = `+${phoneNumbers[i]}`;
        }

        results.push(phoneNumbers[i]);
      }

      return phoneNumbers;
    } catch (error) {
      throw new Error(error);
    }
  }

  async criterialChangeRequest({ company_name, token, email }) {
    await this.queueEmail({
      filePath: 'criteriaChangeRequest.ejs',
      data: {
        company_name,
      },
      email,
      subject: 'Criteria Change Request',
      title: 'LSETF - Criteria Change Request',
    });
  }

  async criterialChangeApproved({ company_name, token, email }) {
    await this.queueEmail({
      filePath: 'criteriaChangeApprove.ejs',
      data: {
        company_name,
      },
      email,
      subject: 'Criteria Change Approved!',
      title: 'LSETF - Criteria Change Approved!',
    });
  }
  async criterialChangeDeclined({ company_name, token, email }) {
    await this.queueEmail({
      filePath: 'criteriaChangeDecline.ejs',
      data: {
        company_name,
      },
      email,
      subject: 'Criteria Change Declined!',
      title: 'LSETF - Criteria Change Declined!',
    });
  }
//forgot password
  async sendForgePasswordLink({ name, token, email }) {
    await this.sendEmail({
      templatePath: '../../views/emails/forgetPassword.ejs',
      data: {
        name,
        token,
        title: 'LSETF - Reset Password',
        url: this.config.get('server.app.frontend_uri'),
      },
      email,
      subject: 'Reset password',
      title: 'Reset Password',
    });
  }

  async sendChangePasswordMail({ name, email }) {
    await this.queueEmail({
      filePath: 'changedPassword.ejs',
      data: {
        name,
      },
      email,
      subject: 'Change password',
      title: 'LSETF - Change Password',
    });
  }

  async sendSignupMail({
    password,
    email,
    name,
    user_type: type,
    lassra_number,
    program_name,
  }) {
    let templatePath = '',
      fileName = '';
    let subject = '';
    let title = 'LSETF - Congratulations!';
    if (
      type === 'staff' ||
      type === 'vtc' ||
      type === 'employer' ||
      type === 'superAdmin' ||
      type === 'stakeholder'
    ) {
      templatePath = '../../views/emails/adminSignup.ejs';
      fileName = 'adminSignup.ejs';
    } else if (type === 'screeningPartner') {
      templatePath = '../../views/emails/screeningPartnerSignup.ejs';
      fileName = 'screeningPartnerSignup.ejs';
    } else if (type === 'alumni') {
      templatePath = '../../views/emails/applicantSignup.ejs';
      fileName = 'applicantSignup.ejs';
    }
    if (type === 'staff') {
      subject = 'Account Opening (Admin)';
    } else if (type === 'vtc') {
      subject = 'Account Opening (VTC)';
    } else if (type === 'screeningPartner') {
      subject = 'Account Opening (Screening Partner)';
    } else if (type === 'employer') {
      subject = 'Account Opening (Employer)';
    } else if (type === 'alumni') {
      subject = 'Account Opening (Alumni)';
    }
    if (type === 'superAdmin') {
      subject = 'Account Opening (Super Admin)';
    }
    if (type === 'stakeholder') {
      subject = 'Account Opening (Stakeholder)';
    }

    await this.queueEmail({
      filePath: fileName,
      email,
      subject,
      title,
      data: { name, password, email, lassra_number, program_name },
    });
  }

  async exportData({ company_name, token, email }) {
    await this.queueEmail({
      filePath: 'exportCsv.ejs',
      data: {
        company_name,
      },
      email,
      subject: 'Criteria Change Request',
      title: 'LSETF - Criteria Change Request',
    });
  }

  async processedApplicationsEmail({ name, email }) {
    await this.queueEmail({
      filePath: 'exportCsv.ejs',
      data: {
        name,
      },
      email,
      subject: 'Criteria Change Request',
      title: 'Criteria Change Request',
    });
  }

  /**
   *
   * @param {string} templatePath
   * @param {string} email
   * @param {object} data
   * @param {string} subject
   * @param {string} title
   */
  async sendEmailToQueue({ templateFileName, email, data, subject, title }) {
    await this.queueEmail({
      filePath: templateFileName,
      data,
      email,
      subject,
      title,
    });
  }

  async jobApplicationApproved({ companyName, name, jobTitle, email }) {
    console.log({ companyName, name, jobTitle, email });
    await this.queueEmail({
      filePath: 'jobApplicationApproved.ejs',
      data: {
        companyName,
        name,
        jobTitle,
      },
      email,
      subject: 'Job Application Status',
      title: 'LSETF - Congratulations!',
    });
  }

  async jobApplicationDeclined({ companyName, name, jobTitle, email }) {
    console.log({ companyName, name, jobTitle, email });
    await this.queueEmail({
      filePath: 'jobApplicationDeclined.ejs',
      data: {
        companyName,
        name,
        jobTitle,
      },
      email,
      subject: 'Job Application Status',
      title: 'LSETF - Job Application Status',
    });
  }

  async applyForJob({ companyName, name, jobTitle, email }) {
    console.log({ companyName, name, jobTitle, email });
    await this.queueEmail({
      filePath: 'jobApplicationDeclined.ejs',
      data: {
        companyName,
        name,
        jobTitle,
      },
      email,
      subject: 'Job Application Notification ',
      title: 'LSETF - Job Application Notification ',
    });
  }
  async acceptJobOffer({ companyName, name, jobTitle, email }) {
    await this.queueEmail({
      filePath: 'acceptJobOffer.ejs',
      data: {
        companyName,
        name,
        jobTitle,
      },
      email,
      subject: 'Job Offer Accepted',
      title: 'Job Offer Accepted',
    });
  }
  async declinedJobOffer({ companyName, name, jobTitle, email }) {
    await this.queueEmail({
      filePath: 'declinedJobOffer.ejs',
      data: {
        companyName,
        name,
        jobTitle,
      },
      email,
      subject: 'Job Offer Declined',
      title: 'Job Offer Declined',
    });
  }

  async scheduleJobInterview({
    companyName,
    name,
    jobTitle,
    email,
    interviewDate,
    interviewTime,
  }) {
    await this.queueEmail({
      filePath: 'scheduleJobInterview.ejs',
      data: {
        companyName,
        name,
        jobTitle,
        interviewDate,
        interviewTime,
      },
      email,
      subject: 'Job Interview Scheduled ',
      title: 'LSETF - Interview Scheduled ',
    });
  }

  async acceptJobInterview({
    companyName,
    name,
    jobTitle,
    email,
    interviewDate,
    interviewTime,
  }) {
    await this.queueEmail({
      filePath: 'acceptJobInterview.ejs',
      data: {
        companyName,
        name,
        jobTitle,
        interviewDate,
        interviewTime,
      },
      email,
      subject: 'Job Interview Schedule Accepted',
      title: 'LSETF - Interview Schedule Accepted',
    });
  }

  async declinedJobInterview({
    companyName,
    name,
    jobTitle,
    email,
    interviewDate,
    interviewTime,
  }) {
    await this.queueEmail({
      filePath: 'declinedJobInterview.ejs',
      data: {
        companyName,
        name,
        jobTitle,
        interviewDate,
        interviewTime,
      },
      email,
      subject: 'Job Interview Schedule Declined',
      title: 'LSETF - Interview Schedule Declined',
    });
  }

  async studentOnboardingMail({
    name,
    vtc_address,
    sector_name,
    course_name,
    email,
    program_name,
  }) {
    await this.queueEmail({
      filePath: 'studentOnboardingMail.ejs',
      data: {
        name,
        vtc_address,
        sector_name,
        course_name,
        program_name,
      },
      email,
      subject: 'Candidate Shortlisted',
      title: 'LSETF - Candidate Shortlisted',
    });
  }

  async declineApplicationScreened({ name, email, program_name }) {
    await this.queueEmail({
      filePath: 'declinedApplicationScreening.ejs',
      data: {
        name,
        program_name,
      },
      email,
      subject: 'Application Declined',
      title: 'LSETF - Application Declined',
    });
  }

  async scheduleScreeingInterview({
    name,
    email,
    course_name,
    instruction,
    date,
    time,
    location,
    contact_number
  }) {
    await this.queueEmail({
      filePath: 'scheduleScreeningInterview.ejs',
      data: {
        name,
        instruction,
        course_name,
        date,
        time,
        location,
        contact_number
      },
      email,
      subject: 'Screening Scheduled',
      title: 'LSETF - Screening Scheduled',
    });
  }

   generateHTML(filename, options = {}) {
    const html = ejs.renderFile(
      `${__dirname}/../../views/emails/${filename}.ejs`,
      options
    );
    const inlined = juice(html);
    return inlined;
  };

  // mailSendingService(payload) {
  //   return new Promise(async (resolve, reject) => {
  //     const text = htmlToText.fromString(payload.htmlMessage);
  //     const mailOptions = {
  //       FromEmail: process.env.SMTP_FROM_EMAIL,
  //       FromName: process.env.SMTP_FROM_NAME,
  //       Subject: payload.subject,
  //       'Text-part': text,
  //       'Html-part': payload.htmlMessage,
  //       Recipients: payload.recepients
  //     };
  //     try {
  //       const request = await mailjet.post('send').request(mailOptions);
  //       return resolve(request);
  //     } catch (err) {
  //       console.log(err);
  //       if (err) return reject(err);
  //     }
  //   });
  // }

}

module.exports = NotificationService;
