const BaseController = require('./Base');
const companyType = require('./../data/companyType');
class AppController extends BaseController {
  constructor({ appFactory }) {
    super();
		this.appFactory = appFactory;
	}
  async index(req, res) {
    const data = this.appFactory.getApp();

		return AppController.success(data, req, res);
	}

  async companyType(req, res) {
    return AppController.success(companyType, req, res);
	}
  async status(req, res) {
    return AppController.success(this.appFactory.status(req), req, res);
	}

  /*generator*/
}

module.exports = AppController;
