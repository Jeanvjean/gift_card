import BaseController from "./Base";

class TransactionController extends BaseController {
    constructor({transactionFactory}) {
        super()
        this.transactionFactory = transactionFactory;
    }

    async deleteTransactions(req, res) {
        const data = await this.transactionFactory.deleteTransactions(req);
        return TransactionController.success(data, req, res);
    }

    async fetchTransactions(req, res) {
        const data = await this.transactionFactory.fetchTransactions(req);
        return TransactionController.success(data, req, res);
    }

    async transactionDetails(req, res) {
        const data = await this.transactionFactory.transactionDetails(req);
        return TransactionController.success(data, req, res);
    }

}

module.exports = TransactionController;