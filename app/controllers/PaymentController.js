import BaseController from "./Base";

class PaymentController extends BaseController {
    constructor({paymentFactory}) {
        super()
        this.paymentFactory = paymentFactory;
    }

    async initialiatePaystackCard(req, res){
        const data = await this.paymentFactory.initialiatePaystackCard(req);
        return PaymentController.success(data, req, res);
    }

    async paystackChargeCard(req, res) {
        const data = await this.paymentFactory.paystackChargeCard(req);
        return PaymentController.success(data, req, res);
    }

    async initStripeCard(req, res) {
        const data = await this.paymentFactory.initStripeCard(req);
        return PaymentController.success(data, req, res);
    }

    async stripeCompleteInitCard(req, res){
        const data = await this.paymentFactory.stripeCompleteInitCard(req);
        return PaymentController.success(data, req, res);
    }

    async stripeChargeCard(req, res) {
        const data = await this.paymentFactory.stripeChargeCard(req);
        return PaymentController.success(data, req, res);
    }

    async deleteCard(req, res){
        const data = await this.paymentFactory.deleteCard(req);
        return PaymentController.success(data, req, res);
    }

    async fetchBanks(req, res) {
        const data = await this.paymentFactory.fetchBanks(req);
        return PaymentController.success(data, req, res);
    }

    async buyGiftyCard(req, res) {
        const data = await this.paymentFactory.buyGiftyCard(req);
        return PaymentController.success(data, req, res)
    }
    
}

module.exports = PaymentController;