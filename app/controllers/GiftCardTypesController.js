import BaseController from './Base';

class GiftCardTypesController extends BaseController {
    constructor({giftCardTypesFactory}) {
        super()
        this.giftCardTypesFactory = giftCardTypesFactory;
    }

    async createGiftCard(req, res){
        const data = await this.giftCardTypesFactory.create(req);
        return GiftCardTypesController.success(data, req, res);
    }

    async fetchGiftCards(req, res){
        const data = await this.giftCardTypesFactory.fetchCards(req);
        return GiftCardTypesController.pagination(data, req, res);
    }

    async cardDetails(req, res) {
        const data = await this.giftCardTypesFactory.cardDetails(req)
        return GiftCardTypesController.success(data, req, res);
    }

    async deleteCard(req, res) {
        const data = await this.giftCardTypesFactory.deleteCard(req);
        return GiftCardTypesController.success(data, req, res);
    }

    async updateCard(req, res) {
        const data = await this.giftCardTypesFactory.updateCard(req);
        return GiftCardTypesController.success(data, req, res);
    }
}

module.exports = GiftCardTypesController;