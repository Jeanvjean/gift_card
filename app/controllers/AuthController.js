const BaseController = require('./Base');

class AuthController extends BaseController {
  constructor({ authFactory }) {
    super();
		this.authFactory = authFactory;
	}

  async signup(req, res) {
    const { user } = await this.authFactory.signup(req);

		return AuthController.success(user, req, res);
	}

  async superadminSignup(req, res){
    const data = await this.authFactory.superadminSignup(req);
    return AuthController.success(data, req, res);
  }

  async adminSignup(req, res) {
    const data = await this.authFactory.adminSignup(req);
    return AuthController.success(data, req, res);
  }

  async login(req, res) {
    const login = await this.authFactory.login(req);

		return AuthController.success(login, req, res);
	}

  async forgetPassword(req, res) {
    const data = await this.authFactory.forgetPassword(req);
		return AuthController.success(data, req, res);
	}

  async resetPassword(req, res) {
    const data = await this.authFactory.resetPassword(req);
		return AuthController.success(data, req, res);
	}

  async changePassword(req, res) {
    const data = await this.authFactory.changePassword(req);
		return AuthController.success(data, req, res);
	}

  /*generator*/
}

module.exports = AuthController;
