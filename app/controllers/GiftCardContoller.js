import BaseController from './Base';


class GiftCardConttroller extends BaseController {
    constructor({ giftCardsFactory }) {
        super()
        this.giftCardsFactory = giftCardsFactory;
    }

    async fetchGiftCards(req, res) {
        const data = await this.giftCardsFactory.fetchGiftCards(req);
        return GiftCardConttroller.success(data, req, res);
    }

    async redeemGiftCard(req, res) {
        const data = await this.giftCardsFactory.redeemGiftCard(req);
        return GiftCardConttroller.success(data, req, res);
    }
}

module.exports = GiftCardConttroller;