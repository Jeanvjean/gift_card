const Queue = require('bull');
const { QUEUE } = require('../constants');

const { parseResultCsv } = require('./result_parser');

class ResultCacheQueue {
  constructor({ config, errors }) {
    this.config = config;
    this.errors = errors;
    this.queueClient = {};
    this.name = '';
  }

  processResultUpload({ body }) {
    this.name = QUEUE.RESULT_UPLOAD_NAME;

    this.queueClient[this.name] = new Queue(
      this.name,
      this.config.get('redis.url')
    );
    this.queueClient[this.name].add(body);
    console.log('done');
    return this;
  }

  async processJob() {
    console.log('process job');
    switch (this.name) {
      case QUEUE.RESULT_UPLOAD_NAME:
        await this.processResultUploadJob();

        return 'done';
        break;
    }
  }

  async processResultUploadJob() {
    if (!this.queueClient[this.name]) {
      return 'done';
    }
    this.queueClient[this.name].process(async (job, done) => {
      try {
        await parseResultCsv(job.data);
        done();
      } catch (err) {
        console.log({ err });
      }
    });
  }
}

module.exports = ResultCacheQueue;
