const path = require('path');
const fastcsv = require('fast-csv');
const through2 = require('through2');

const parseResultCsv = ({ bucket_url }) => {
  return new Promise(async (resolve, reject) => {
    try {
      // let params = {
      //   Bucket: file.bucket,
      //   Key: file.key,
      // };

      const https = require('https');
      https.get(bucket_url, (stream) => {
        stream
          .pipe(fastcsv.parse({ headers: true }))
          .on('error', (error) => {
            return resolve();
            // return reject(`invalid file passed ${file.key}`);
          })
          .on('data-invalid', async function (row) {
            throw new Error('data-invI thalid');
          })
          .pipe(
            through2({ objectMode: true }, async (row, enc, cb) => {
              try {
                console.log(row);
                // process here
              } catch (processError) {
                // catch error thrown
              }
            })
          )
          .on('data', function (row) {
            // console.log('data')
          })
          .on('end', async (rowCount) => {
            // Successfully uploaded, send mail to screen partners
          });
      });
    } catch (err) {
      return reject(err);
    }
  });
};

module.exports = {
  parseResultCsv,
};
