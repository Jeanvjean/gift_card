const jwt = require('jsonwebtoken');
const generator = require('generate-password');
const { addMonths, endOfToday, format, addMinutes } = require('date-fns');
const { v4: uuidv4 } = require('uuid');
const bcrypt = require('bcrypt');
const uuidAPIKey = require('uuid-apikey');
const { Parser } = require('json2csv');
const randomNumber = require('random-number');
const crypto = require('crypto');

class Helper {
  constructor({
    config,
    errors,
    database,
    queries,
    // notificationService,
  }) {
    this.config = config;
    this.errors = errors;
    this.database = database;
    this.queries = queries;
    // this.notificationService = notificationService;
  }

  generatePassword() {
    return generator.generate({
      length: 10,
      numbers: true,
    });
  }

  generateUniqueID() {
    return randomNumber.generator({
      min: 10000000000,
      max: 99999999999,
      integer: true,
    })();
  }

  async generatePasswordHash({ password }) {
    try {
      const hash = await bcrypt.hash(
        password,
        this.config.get('server.app.round_salt')
      );
      return hash;
    } catch (err) {
      throw new Error(err);
    }
  }

  async comparePasswordHash({ plainPassword, hashPassword }) {
    try {
      const match = await bcrypt.compare(plainPassword, hashPassword);
      return match;
    } catch (err) {
      throw new Error(err);
    }
  }

  async generateResetPasswordLink() {
    try {
      const { uuid } = uuidAPIKey.create();
      const expiredAt = addMinutes(new Date(), 15);

      return { token: uuid, uuid, expiredAt };
    } catch (err) {
      throw new Error(err);
    }
  }

  async formResetPasswordLink() {
    try {
      const { token, uuid, expiredAt } = await this.generateResetPasswordLink();

      const link = `${this.config.get(
        'server.app.frontend_uri'
      )}?token=${token}`;

      return { token: uuid, expiredAt, url: link };
    } catch (err) {
      throw new Error(err);
    }
  }

  async generateJwtToken(payload) {
    try {
      const token = jwt.sign(
        {
          data: payload,
        },
        this.config.get('server.jwt.secret'),
        { expiresIn: this.config.get('server.jwt.expires') }
      );

      return token;
    } catch (err) {
      throw new this.errors.InternalServer(err);
    }
  }

  async verifyToken(token) {
    try {
      const decoded = jwt.verify(token, this.config.get('server.jwt.secret'));

      return decoded;
    } catch (err) {
      throw new this.errors.InternalServer(err);
    }
  }

  generateToken = num =>
    new Promise((resolve, reject) => {
      crypto.randomBytes(num || 16, (err, buffer) => {
        if (err) reject(err);
        const token = buffer.toString('hex');
        resolve(token);
      });
    });
	

  generateKey() {
    try {
      const generatedKey = uuidAPIKey.create();

      const publicKey = `PUBLIC-${generatedKey.uuid.toUpperCase()}`;
      const secretKey = `SECRET-${generatedKey.apiKey}`;

      return { public_key: publicKey, secret_key: secretKey };
    } catch (err) {
      throw new this.errors.InternalServer(err);
    }
  }

  getLimitOffset({ limit = 20, page = 1 }) {
    try {
      const offset = parseInt(
        parseInt(limit) * parseInt(page) - parseInt(limit)
      );

      return { offset, limit, page };
    } catch (err) {
      throw new Error(err);
    }
  }

  toCSV({ fields, data }) {
    const parser = new Parser({ fields });
    return parser.parse(data);
  }
  toTwoValue(value) {
    if (String(value).length === 1) {
      return `0${value}`;
    }
    return value;
  }
  formatDate(dateValue) {
    const date = new Date(dateValue);
    return `${date.getFullYear()}/${this.toTwoValue(
      date.getMonth() + 1
    )}/${this.toTwoValue(date.getDate())}`;
  }
}

module.exports = Helper;
