const { setDatabase } = require('./rabbitmqDb');
const options = {};

const database = require('../../providers/database').boot(options);
setDatabase(database);
module.exports = database;
