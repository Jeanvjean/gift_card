var aws = require('aws-sdk');
var multerS3 = require('multer-s3');
const config = require('../../config');
const fs = require('fs');

const uploadConfig = {
  applicantProfilePhoto: {
    dir: 'application/profilePhoto',
  },
  applicantLassraID: {
    dir: 'applicantion/lassraId',
  },
  applicantEducationCertificate: {
    dir: 'applicantion/educationCertificate',
  },
  applicantEducationCertificate: {
    dir: 'applicantion/educationCertificate',
  },
  applicantVocationalCertificate: {
    dir: 'applicantion/vocationalCertificate',
  },
};

function getUploadType({ type }) {
  const { dir } = uploadConfig[type];
	return { dir };
}

const s3 = new aws.S3({
  endpoint: new aws.Endpoint('nyc3.digitaloceanspaces.com'),
  accessKeyId: config.get('DOSpacesConfig.accessKey'),
  secretAccessKey: config.get('DOSpacesConfig.secretKey'),
});

// const uploadFile = (fileName) => {
//   // Read content from the file
//   const fileContent = fs.readFileSync(fileName);
//   console.log(fileContent)
//   // Setting up S3 upload parameters
//   const params = {
//       Bucket:  config.get('DOSpacesConfig.bucket'),
//       Key: fileName, // File name
//       Body: fileContent
//   };

//   // Uploading files to the bucket
//   s3.upload(params, function(err, data) {
//       if (err) {
//           throw err;
//       }
//       console.log(`File uploaded successfully. ${data.Location}`, data);
//       // return data;
//   });
// };

const storage = multerS3({
  s3: s3,
  bucket: config.get('DOSpacesConfig.bucket'),
  acl: 'public-read',
  metadata: function (req, file, cb) {
    cb(null, { fieldName: file.fieldname });
	},
  key: function (req, file, cb) {
    // const { type } = req.body;
    // console.log(req, file)
		// let splitedType = type.split('_');
		// if (splitedType.length > 1) {
    //   splitedType.pop();
		// }

    cb(
      null,
      `${config.get('server.app.environment')}/uploads/${Date.now().toString()}_${file.originalname}`
    );
  },
});

module.exports = { storage };
