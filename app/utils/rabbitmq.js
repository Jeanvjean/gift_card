const config = require('../../config');

const amqp = require('amqplib');

let connectionString;
switch (config.get('server.app.environment')) {
  case 'production':
    connectionString = config.get('rabbitmq.url');
	// connectionString = `amqp://${config.get('rabbitmq.username')}:${config.get('rabbitmq.password')}@${config.get('rabbitmq.domain')}:${config.get('rabbitmq.port')}`;
	break;
  case 'development':
    connectionString = config.get('rabbitmq.url');
	// connectionString = `amqp://${config.get('rabbitmq.username')}:${config.get('rabbitmq.password')}@${config.get('rabbitmq.domain')}:${config.get('rabbitmq.port')}`;
	break;
  default:
    connectionString = config.get('rabbitmq.url');
	// connectionString = `amqp://${config.get('rabbitmq.username')}:${config.get('rabbitmq.password')}@${config.get('rabbitmq.domain')}:${config.get('rabbitmq.port')}`;
	break;
}

// console.log({ connectionString });
const connection = async () => {
  try {
    const amqpconnection = await amqp.connect(connectionString);
		const channel = await amqpconnection.createConfirmChannel();
		// console.log('open queue');
		return {
      connection: amqpconnection,
      channel,
    };
  } catch (error) {
    // console.log(error);
		throw new Error(error);
	}
};
//abp_monitor
const rabbitmqArchitecture = (worker) =>
  new Promise((resolve, reject) => {
    // console.log(worker)
    try {
      const exchange = 'aku.lsesp_monitor.exchange';
			console.log({ exchange, worker });
			switch (worker) {
        case 'email_sending_queue':
          resolve({
            queue: 'email_sending.queue',
            exchange,
            routingKey: 'send_email',
          });
				break;
        case 'export_queue':
          resolve({
            queue: 'export_data_queue',
            exchange,
            routingKey: 'export_data',
          });
				break;
        case 'update_student_queue':
          resolve({
            queue: 'update_student.queues',
            exchange,
            routingKey: 'update_student.send',
          });
				break;

          break;
        default:
          throw new Error('Invalid queue: Something bad happened!');
			}
    } catch (error) {
      reject(error);
		}
  });

module.exports = { rabbitmq: connection, rabbitmqArchitecture };
