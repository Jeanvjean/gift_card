const config = require('./../../config');

class Response {
  constructor(domain) {
    this.domain = domain || config.get('server.app.domain');
	}

  success(path, response, status = 'success') {
    const current_url = `${this.domain}${path}`;
		// const messages = err.message || err.error.message;

		if (response.length <= 0) {
      return new Error('Error: Object (data) is required!');
		}

    const { message, data } = response;

		return {
      current_url,
      message,
      data,
      status: 'Success',
    };
  }

  error(req, res, err) {
    console.log(err);
		const current_url = `${this.domain}${req.originalUrl}`;
		const message = err.message || 'Bad Request';
		const code = err.statusCode || err.code || 500;
		if (!message) {
      return new Error('Error: Object (message) is required!');
		}

    return res.status(code).json({
      current_url,
      message,
      // name: err.name || err.error.name,
      status_code: code,
    });
	}

  validation_error(req, res, err) {
    const current_url = `${this.domain}${req.originalUrl}`;
		const message = err.message || err.error.message;
		const code = err.statusCode || 422;

		if (!message) {
      return new Error('Error: Object (message) is required!');
		}

    return res.status(422).json({
      current_url,
      message,
      name: err.name || err.error.name,
      errors: err.errors || err.error.details,
      code,
    });
	}

  pagination(path, response, status = 'success') {
    // console.log(response)
    const current_url = `${this.domain}${path}`;
      // const messages = err.message || err.error.message;
      if (response.length <= 0) {
        return new Error('Error: Object (data) is required!');
      }

      const {
          message, data, params, query
      } = response;
      // let total = 10;

      const total = parseInt(data.total);
      const page =
    parseInt(params.page || query.page || 1) >= total ?
        total :
        parseInt(params.page || query.page || 1);
      const limit = parseInt(params.limit || query.limit || config.get('server.app.pagination_size'));
      const last_page = Math.ceil(total / limit) > 0 ? Math.ceil(total / limit) : 1;
      const next_page = page > last_page ? page : page + 1;
      const previous_page = page - 1 < 0 ? 0 : page - 1;
      data.data = data.data.slice((page - 1) * limit, page * limit);
      const meta = {
          page,
          next_page,
          limit,
          total,
          last_page,
          previous_page
      };
      
      return {
          current_url,
          message,
          data,
          meta,
          status: 'Success'
      };
  }
}

module.exports = new Response();
