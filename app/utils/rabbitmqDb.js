let database = null;

const getDatabase = () => {
  return database;
}

const setDatabase = (db) => {
  database = db;
}

module.exports = { getDatabase, setDatabase };
