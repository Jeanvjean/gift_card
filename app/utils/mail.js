const { dispatchEmailNotification } = require('./notifications');
const fs = require('fs');
const ejs = require('ejs');
const path = require('path');
const { notification } = require('../../config/notification');
const config = require('./../../config');

const sendEmail = async ({
  to,
  from,
  subject,
  bcc,
  cc,
  filePath,
  title,
  payload,
  attachment = false,
  fileName = '',
  fileType = '',
  content = '',
}) => {
  try {
    const html = await ejs.renderFile(
      path.join(__dirname, `/../../views/emails/${filePath}`),
      payload
    );
    const notificationPayload = {
      to,
      from: title
        ? `${title} <${config.get('notification.email.from_mail')}>`
        : config.get('notification.email.from_mail'),
      subject,
      bcc: [
        config.get('notification.email.from_mail'),
        'israel.love4life@gmail.com',
        'wisdomu@akufintech.com',
        // 'fjohn087@gmail.com'
      ],
      cc,
      attachment,
      html,
    };

    console.log(notificationPayload);

    if (attachment) {
      notificationPayload.fileName = fileName;
      notificationPayload.fileType = fileType;
      notificationPayload.content = content;
    }
    console.log(notificationPayload);
    await dispatchEmailNotification(notificationPayload);

    return 'send successfully';
  } catch (err) {
    throw new Error(err);
  }
};

const generateCode = () => Math.floor(100000 + Math.random() * 900000);

const fileGetContent = (filePath) =>
  new Promise(async (resolve, reject) => {
    try {
      const fileContent = fs
        .readFileSync(path.resolve(filePath))
        .toString('utf8');

      resolve(fileContent);
    } catch (err) {
      console.log({ err4: err });
      reject(err);
    }
  });

const getFileSize = (file) => {
  const stats = fs.statSync(file);
  const fileSizeInBytes = stats.size;
  // Convert the file size to megabytes (optional)
  const fileSizeInMegabytes = fileSizeInBytes / 1000000.0;

  return fileSizeInMegabytes;
};

const base64EncodeFile = (file) =>
  new Promise(async (resolve, reject) => {
    try {
      const bitmap = fs.readFileSync(file);
      // convert binary data to base64 encoded string
      const base64Content = Buffer.from(bitmap).toString('base64');

      resolve(base64Content);
    } catch (err) {
      reject(err);
    }
  });
module.exports = {
  sendEmail,
  fileGetContent,
  generateCode,
  base64EncodeFile,
  getFileSize,
};
