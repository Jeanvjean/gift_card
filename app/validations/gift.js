import Joi from '@hapi/joi';

const createCardTypeSchema = Joi.object().keys({
    name: Joi.string().required(),
    min_price: Joi.string().required(),
    max_price: Joi.string().required(),
    currency: Joi.string()
});

const updateCardTypeSchema = Joi.object().keys({
    min_price: Joi.string(),
    max_price: Joi.string(),
    currency: Joi.string()
});

module.exports = {
    createCardTypeSchema,
    updateCardTypeSchema
}