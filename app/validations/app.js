const Joi = require('@hapi/joi');

const createSignupSchema = Joi.object().keys({
  email: Joi.string().required().min(5),
  // password: Joi.string().required().min(6),
  // confirmPassword: Joi.string().required().min(6),
});

/*generator*/

module.exports = {
  createSignupSchema,
  /*exporterGenerator*/
};
