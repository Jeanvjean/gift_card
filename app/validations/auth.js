const Joi = require('@hapi/joi');

const createSignupSchema = Joi.object().keys({
  password: Joi.string().required().min(6)
  .regex(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/)
  .error(()=>{
    return {
      message: 'Password must be at least six(6) character long and most contain at least 1 letter, 1 number and 1 special character'
  };
  }),
  email: Joi.string().email().required(),
  name:Joi.string()
});

const adminSignupSchema = Joi.object().keys({
  email: Joi.string().email().required(),
  name:Joi.string()
});

const createLoginSchema = Joi.object().keys({
  email: Joi.string().required().min(5),
  password: Joi.string().required().min(6),
});

const createForgetPasswordSchema = Joi.object().keys({
  email: Joi.string().required().min(5),
});

const createResetPasswordSchema = Joi.object().keys({
  token: Joi.string().required().min(5),
  password: Joi.string().required().min(6)
  .regex(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{6,}$/)
  .error(()=>{
    return {
      message: 'Password must be at least six(6) character long and most contain at least 1 letter, 1 number and 1 special character'
  };
  })
});

const createChangePasswordSchema = Joi.object().keys({
  oldPassword: Joi.string().required().min(5),
  newPassword: Joi.string().required().min(5),
  confirmPassword: Joi.ref('newPassword'),
});

/*generator*/

module.exports = {
  createLoginSchema,
  createForgetPasswordSchema,
  createResetPasswordSchema,
  createChangePasswordSchema,
  createSignupSchema,
  adminSignupSchema
  /*exporterGenerator*/
};
