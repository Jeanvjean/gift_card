const {
  USER_TYPE_SUPER_ADMIN,
  USER_TYPE_STAFF,
  USER_TYPE_VTC,
  USER_TYPE_SCREENING_PARTNER,
  USER_TYPE_EMPLOYER,
  FULL_TIME_EMPLOYMENT,
  PART_TIME_EMPLOYMENT,
  CONTRACT_EMPLOYMENT,
  COMMISSION_EMPLOYMENT,
  STAKEHOLDER,
} = require('../constants');

const userTypes = {
  [USER_TYPE_SUPER_ADMIN]: 'Super Admin',
  [USER_TYPE_STAFF]: 'LSESP Staff',
  [USER_TYPE_VTC]: 'Vocational Training Center',
  [USER_TYPE_SCREENING_PARTNER]: 'Screening Partner',
  [USER_TYPE_EMPLOYER]: 'Employer',
  [STAKEHOLDER]: 'stakeholder'
};

const employmentType = {
  [FULL_TIME_EMPLOYMENT]: 'Full Time',
  [PART_TIME_EMPLOYMENT]: 'Part Time',
  [CONTRACT_EMPLOYMENT]: 'Contract',
  [COMMISSION_EMPLOYMENT]: 'Commission',
};

module.exports = { userTypes, employmentType };
