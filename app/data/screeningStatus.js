module.exports = [
  { id: 'approved', label: 'Approved' },
  { id: 'declined', label: 'Declined' },
  { id: 'referred', label: 'Referred' },
  { id: 'pending', label: 'Pending' },
  { id: 'absent', label: 'Absent' },
];
