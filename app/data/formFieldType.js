module.exports = [
  {
    name: 'input',
    label: 'Single Line Text',
    validation: ['required', 'email', 'number', 'tel'],
  },
  { name: 'textarea', label: 'Paragraph Text', validation: ['required'] },
  { name: 'select', label: 'Dropdown', validation: ['required'] },
  {
    name: 'checkbox',
    label: 'Multiple Choice',
    validation: ['required'],
  },
  { name: 'date_picker', label: 'Date picker', validation: ['required'] },
  { name: 'time_picker', label: 'Time picker', validation: ['required'] },
  {
    name: 'date_time_picker',
    label: 'DateTimepicker',
    validation: ['required'],
  },
  { name: 'file', label: 'File', validation: ['required'] },
];
