module.exports = [
  {
    name: 'Scheduled',
    slug: 'scheduled',
  },
  {
    name: 'Completed',
    slug: 'completed',
  },
  {
    name: 'Cancelled',
    slug: 'cancelled',
  },
  {
    name: 'Declined',
    slug: 'declined',
  },
]
