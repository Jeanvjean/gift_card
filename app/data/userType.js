module.exports = [
  {
    name: 'Super Admin',
    slug: 'superAdmin',
  },
  {
    name: 'LSESP STAFF',
    slug: 'staff',
  },
  {
    name: 'Vocational Training Center',
    slug: 'vtc',
  },
  {
    name: 'Screening Partner',
    slug: 'screeningPartner',
  },
  {
    name: 'Employer',
    slug: 'employer',
  },
  {
    name:"Stakeholder",
    slug:"stakeholder"
  }
];
