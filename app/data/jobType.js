module.exports = [
  {
    name: 'Full time',
    slug: 'fullType',
  },
  {
    name: 'Part time',
    slug: 'partTime',
  },
  {
    name: 'Contract',
    slug: 'contract',
  },
  {
    name: 'Commission',
    slug: 'commission',
  },
];
