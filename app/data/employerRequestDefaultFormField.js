module.exports = [
  {
    label: 'Company Name',
    name: 'company_name',
    type: 'input',
    detail_type: 'application',
    validation: 'required',
    field_order: 1,
  },
  {
    label: 'Company Type',
    name: 'company_type',
    type: 'select',
    detail_type: 'application',
    validation: 'required',
    field_order: 2,
  },
  {
    label: 'Contact Person First Name',
    name: 'first_name',
    type: 'input',
    detail_type: 'application',
    validation: 'required',
    field_order: 3,
  },
  {
    label: 'Contact Person Last Name',
    name: 'last_name',
    type: 'input',
    detail_type: 'application',
    validation: 'required',
    field_order: 4,
  },
  {
    label: 'Email',
    name: 'email',
    type: 'input',
    detail_type: 'application',
    validation: 'required,email',
    field_order: 5,
  },
  {
    label: 'Phone Number',
    name: 'phone_number',
    type: 'input',
    detail_type: 'application',
    validation: 'required,tel',
    field_order: 6,
  },
  {
    label: 'Address',
    name: 'address',
    type: 'textarea',
    detail_type: 'application',
    validation: 'required',
    field_order: 8,
  },
  {
    label: 'Local Government Area',
    name: 'local_government_id',
    type: 'select',
    detail_type: 'application',
    validation: 'required',
    field_order: 7,
  },

  {
    label: 'Number of Employee',
    name: 'number_of_employee',
    type: 'select',
    detail_type: 'hiring',
    validation: 'required',
    field_order: 9,
  },
  {
    label: 'Minimum Salary',
    name: 'minimum_salary',
    type: 'select',
    options: `0,50000,100000,150000,200000,250000,300000,350000,400000,450000,500000,550000,600000,650000,700000`,
    detail_type: 'hiring',
    validation: 'number,required',
    field_order: 10,
  },
  {
    label: 'Maximum Salary',
    name: 'maximum_salary',
    type: 'select',
    options: `50000,100000,150000,200000,250000,300000,350000,400000,450000,500000,550000,600000,650000,700000,750000,800000,
    850000,900000,950000,1000000`,
    detail_type: 'hiring',
    validation: 'number,required',
    field_order: 11,
  },
  {
    label: 'Are you ready to hire?',
    name: 'ready_to_hire',
    type: 'select',
    options: 'Yes,No',
    detail_type: 'hiring',
    validation: 'number,required',
    field_order: 11,
  },
  {
    label: 'Required start date',
    name: 'start_date',
    type: 'date_picker',
    detail_type: 'hiring',
    validation: 'required',
    field_order: 12,
  },
  {
    label: 'How you hear about us?',
    name: 'how_you_hear_about_us',
    type: 'select',
    options: 'Social Media, Google search',
    detail_type: 'hiring',
    validation: 'required',
    field_order: 13,
  },
];
