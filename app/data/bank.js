exports.bankNameMatchers = [
    {
      expressions: [/access.*/i, /aces.*/i],
      name: 'Access Bank',
      code: '044',
    },
    {
      expressions: [/citi.*/i, /cit1.*/i, /city.*/i],
      name: 'Citibank Nigeria',
      code: '023',
    },
    {
      expressions: [/diam.*/i, /d1am.*/i, /diom.*/i, /dian.*/i],
      name: 'Diamond Bank',
      code: '063',
    },
    {
      expressions: [/eco.*/i, /eko.*/i, /ek0.*/i, /ec0.*/i],
      name: 'Ecobank Nigeria',
      code: '050',
    },
    {
      expressions: [/enter.*/i, /emter.*/i, /entar.*/i],
      name: 'Enterprise Bank',
      code: '084',
    },
    {
      expressions: [/fidel.*/i, /f1del.*/i, /fide1.*/i, /fid/],
      name: 'Fidelity Bank',
      code: '070',
    },
    {
      expressions: [/first.* b.*/i, /f1rst.* bank/i, /f0rst.* bonk/i],
      name: 'First Bank of Nigeria',
      code: '011',
    },
    {
      expressions: [/first.* c.*/i, /f1rst.* cit.*/i, /f0rst.* citi/i],
      name: 'First City Monument Bank',
      code: '214',
    },
    {
      expressions: [/gtb.*/i, /gtv.*/i, /9tb.*/i, /guar.*/i, /guor.*/i],
      name: 'Guaranty Trust Bank',
      code: '058',
    },
    {
      expressions: [/heri.*/i, /her1.*/i],
      name: 'Heritage Bank',
      code: '030',
    },
    {
      expressions: [/key.*/i],
      name: 'Keystone Bank',
      code: '082',
    },
    {
      expressions: [/mai.*/i],
      name: 'MainStreet Bank',
      code: '014',
    },
    {
      expressions: [/sky.*/i],
      name: 'Skye Bank',
      code: '076',
    },
    {
      expressions: [/stanb.* i.*/i, /stanb.* ib.*/i],
      name: 'Stanbic IBTC Bank',
      code: '221',
    },
    {
      expressions: [/stand.* ch.*/i],
      name: 'Standard Chartered Bank',
      code: '068',
    },
    {
      expressions: [/ste.* b.*/i],
      name: 'Sterling Bank',
      code: '232',
    },
    {
      expressions: [/unio.* b.*/i, /umio.* b.*/i, /un1o.* b.*/i, /uni0.* b.*/i],
      name: 'Union Bank of Nigeria',
      code: '032',
    },
    {
      expressions: [/unite.* b.*/i, /umite.* b.*/i, /un1te.* b.*/i],
      name: 'United Bank For Africa',
      code: '033',
    },
    {
      expressions: [/unity b.*/i, /umity b.*/i, /un1ty b.*/i],
      name: 'Unity Bank',
      code: '215',
    },
    {
      expressions: [/wem.* b.*/i, /wen.* b.*/i],
      name: 'Wema Bank',
      code: '035',
    },
    {
      expressions: [/zen.* b.*/i],
      name: 'Zenith Bank',
      code: '057',
    },
  ];
  