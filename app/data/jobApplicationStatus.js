module.exports = [
  {
    name: 'Pending',
    slug: 'pending',
  },
  {
    name: 'Interviewing',
    slug: 'interviewing',
  },
  {
    name: 'Hired',
    slug: 'hired',
  },
  {
    name: 'Not Hired',
    slug: 'notHired',
  },
  {
    name: 'Declined',
    slug: 'declined',
  },
];
