module.exports = {
  create:
    "INSERT INTO users (email, password, must_change_password) VALUES ($[email], $[password], B'$[must_change_password]') RETURNING id",
  selectRolePermission: `
    SELECT 
        permission.permission 
    FROM 
        role_permission 
            INNER JOIN 
                permission 
            ON role_permission.permission_id =  permission.id
    WHERE role_permission.role_id = $[role_id]`,
  selectById: 'SELECT * FROM users WHERE id = $[id]',
};
