module.exports = {
  create: 
  "INSERT INTO users (email, password, must_change_password) VALUES ($[email], $[password], B'$[must_change_password]') RETURNING *",

  selectByEmail: 
  'SELECT * FROM users WHERE email = $[email]',

//   find:
//   'SELECT * FROM `${table}` WHERE query = $[value]',

  selectById: 
  'SELECT * FROM users WHERE id = $[id]',

  updateUserResetLinkByEmail: 
  `UPDATE 
        users 
    SET 
        password_reset_token = $[password_reset_token], 
        password_reset_token_expiration = $[password_reset_token_expiration] 
    WHERE 
        email = $[email]`,

  selectByToken: 
  `SELECT
        * 
    FROM 
        users 
    WHERE 
        password_reset_token = $[token] 
        AND 
        password_reset_token_expiration >= $[current_time]
    `,

  updatePasswordById: 
  `UPDATE 
            users
        SET
            password = $[password], 
            must_change_password = '0'
        WHERE
            id = $[id]`,

  all: 
  'SELECT email, password, profile_id, role_id FROM cash_bands WHERE deleted_at IS NULL',

  fetchBandByAmount: 
  'SELECT * FROM cash_bands WHERE amount = $[amount] AND programme_id = $[programme_id] LIMIT 1',
};
