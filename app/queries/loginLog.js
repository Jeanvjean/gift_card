module.exports = {
  create:
    'INSERT INTO login_log (user_agent, user_id) VALUES ($[user_agent], $[user_id])',
  selectByEmail: 'SELECT * FROM users WHERE email = $[email]',
  all: 'SELECT email, password, profile_id, role_id FROM cash_bands WHERE deleted_at IS NULL',
  fetchBandByAmount:
    'SELECT * FROM cash_bands WHERE amount = $[amount] AND programme_id = $[programme_id] LIMIT 1',
};
