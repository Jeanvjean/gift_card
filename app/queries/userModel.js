module.exports = {
  create: `
      INSERT INTO 
          user_profile 
              (profile_id, applicant_id, users_id, role_id) 
      VALUES 
          ($[profile_id], $[user_id], $[role_id], $[applicant_id]) RETURNING id`,
};
