const BaseQueries = require('./base');
const AppQueries = require('./app');
const UsersQueries = require('./users');
const LoginLog = require('./loginLog');
const ProfileQueries = require('./profile');
const aclQueries = require('./acl');

module.exports = {
  get base() {
    return BaseQueries;
	},
  get app() {
    return AppQueries;
	},
  get users() {
    return UsersQueries;
	},
  get loginLog() {
    return LoginLog;
	},
  get profile() {
    return ProfileQueries;
	},
  get acl() {
    return aclQueries;
	},
};
