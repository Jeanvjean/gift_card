module.exports = {
  create: `
    INSERT INTO 
        profile 
            (first_name, last_name, email, phone_number, local_government_area_id) 
    VALUES 
        ($[email], $[first_name], $[last_name], $[phone_number], $[local_government_area_id]) RETURNING id`,
};
