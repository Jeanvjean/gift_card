const { USER_TYPE_CLIENT, USER_TYPE_SUPER_ADMIN, USER_TYPE_ADMIN } = require("../constants");
import ResponseTransformer from "../utils/ResponseTransformer";
// const { processApplicantUpload } = require('../utils/applicant');
class AuthFactory {
  constructor(props) {
    const {
      config,
      errors,
      database,
      queries,
      helper,
      notificationService,
      userRepository
      // currentUser,
    } = props;
    this.config = config;
    this.errors = errors;
    this.database = database;
    this.queries = queries;
    this.helper = helper;
    this.notificationService = notificationService;
    this.userRepository = userRepository
    this.currentUser = null;
    try {
      this.currentUser = props.currentUser;
    } catch (e) {}
  }

  async signup({
    body: {
      email,
      password,
      name,
      user_type
    }
  }) {
    const existEmail = await this.userRepository.baseGetAll({
      where: { email: { _eq: email } },
    });

    if (existEmail.length > 0) {
      throw new Error('a user with this email already exists');
    }

    const hashPassword = await this.helper.generatePasswordHash({ password });
    const user = await this.userRepository.baseInsert({
      data: {
        email: email.toLowerCase(),
        password: hashPassword,
        name,
        user_type: user_type || USER_TYPE_CLIENT
      },
    });

    return {
      user: {
        ...user[0],
        password:null
      }
    };
  }

  async superadminSignup({body}) {
    let password = await this.helper.generateToken(4);
    console.log(password)
    const { user } =  await this.signup({body:{
        ...body,
        user_type: USER_TYPE_SUPER_ADMIN,
        password
      }
    });

    await this.notificationService.sendEmail({
      templatePath:'signup_mail',
      data:{
        name: user.name,
        password: password,
        message: `use the password: ${password} to login to your jitaku gift card account`
      },
      email: user.email,
      title:'Signup mail',
      subject:'Signup mail'
    });

    return user;
  }

  async adminSignup({body, authUser}) {
    if(authUser.user_type !== USER_TYPE_SUPER_ADMIN) {
      throw new Error('only super admin has this permision')
    }
    let password = await this.helper.generateToken(4);
    const { user } =  await this.signup({body:{
        ...body,
        user_type: USER_TYPE_ADMIN,
        password
      }
    });

    await this.notificationService.sendEmail({
      templatePath:'signup_mail',
      data:{
        name: user.name,
        password: password,
        message: `use the password: ${password} to login to your jitaku gift card account`,
        title:"Signup mail"
      },
      email: user.email,
      title:'Signup mail',
      subject:'Signup mail'
    });

    return user;
  }

  async login({ body: { email, password } }) {
    const user = await this.userRepository.fetchUserByEmail({
      email: email.toLowerCase()
    });

    // console.log(user);

    if (!user) {
      throw new this.errors.Validation('Invalid login credential');
    }
    const match = await this.helper.comparePasswordHash({
      plainPassword: password,
      hashPassword: user.password,
    });

    if (!match) {
      throw new this.errors.Validation('Invalid login credential');
    }
    const token = await this.helper.generateJwtToken({ id: user.id, email:user.email })
    // console.log(token)
    return {
      description: 'Operation successful',
      user: {...user, password: null},
      token: token,
    };
  }

  /*
   * params payload
   *
   *
   */
  async forgetPassword({ body: { email } }) {
    const users = await this.userRepository.baseGetAll({
      where: { email: { _eq: email } },
    });
    // let name = '';

    const responseMessage = { message: 'Link successfully sent' };

    if (users.length === 0) {
      throw new this.errors.Validation('A user with this email was not found');
    }

    let token = await this.helper.generateJwtToken({id:users[0].id, email:users[0].email});

    let url = `${this.config.get('server.app.frontend_uri')}reset-password/${token}`

    await this.notificationService.sendEmail({
      templatePath:'forget_password',
      data:{
        link: url,
        name: users[0].name,
        message: `click on the link to reset your password`,
        title:"Reset password"
      },
      email: users[0].email,
      title:'Reset password',
      subject:'Forgot password'
    });

    return responseMessage;
  }

  async resetPassword({ body: { token, password } }) {

    let decode = await this.helper.verifyToken(token)

    if (!decode) {
      return ResponseTransformer.error(
        req,
        res,
        new errors.Unauthorize('Invalid/expired Token')
      );
    }
    console.log(decode)
    let { data:{ id, email }, iat, exp } = decode;

    let user = await this.userRepository.fetchUserByEmail({
      email: email
    });

    if(!user) {
      return ResponseTransformer.error('Expired/invalid token')
    }
    let hashPassword = await this.helper.generatePasswordHash({ password })
    await this.userRepository.baseUpdate({
      where:{ email: { _eq: user.email } },
      _set: {
        password: hashPassword
      }
    })
    return { description: 'Operation successful' };
  }

  async changePassword({
    body: { oldPassword, newPassword, confirmPassword },
  }) {
    //TO-DO change password function

    return { description: 'Operation successful' };
  }

  /*generator*/
}

module.exports = AuthFactory;
