const { COMPLETED, PAYSTACK_SERVICE, STRIPE_SERVICE, NOT_DEFAULT } = require('../constants');

const Paystack = require('paystack-api')(process.env.PAYSTACK_SECRET_KEY);

const Stripe = require('stripe')(process.env.STRIPE_SECRET);

class PaymentFactory {
    constructor(props) {
        this.config = props.config;
        this.errors = props.errors;
        this.database = props.database;
        this.queries = props.queries;
        this.helper = props.helper;
        this.transactionRepository = props.transactionRepository
        this.notificationService = props.notificationService;
        this.paymentRepository = props.paymentRepository
        this.giftCardRepository = props.giftCardRepository
        this.userRepository = props.userRepository
        this.currentUser = props.currentUser;
    }

    async initialiatePaystackCard({body, authUser}) {
        let message = "";

        let { transaction_reference, email } = body;
        const response = await Paystack.transaction.verify({
            reference: transaction_reference
        });

        if(response.data && response.data.status == 'success') {

            if(response.data.authorization && response.data.authorization.reusable) {
                let auth = response.data.authorization;
                let card = await this.paymentRepository.baseGetOne({
                    last_4_digits: auth.last4,
                    exp_year: auth.exp_year,
                    exp_month: auth.exp_month,
                    user_id: authUser.id,
                    service:PAYSTACK_SERVICE
                });

                if(!card) {
                    card = await this.paymentRepository.baseInsert({
                        data: {
                            last_4_digits:auth.last4,
                            card_type:auth.card_type,
                            authorization_code:auth.authorization_code,
                            exp_year:auth.exp_year,
                            exp_month:auth.exp_month,
                            holder_name:this.currentUser.name,
                            user_id: authUser.id,
                            default_card: NOT_DEFAULT,
                            transactions:1
                        }
                    })
                    // console.log(card);
                    // return;
                    let transaction = await this.transactionRepository.baseGetOne({
                        where:{ transaction_reference: { _eq: response.data.reference } }
                    });
                    if(!transaction) {
                        let trans = await this.transactionRepository.baseInsert({
                            data:{
                                source:card[0].id,
                                amount: response.data.amount / 100,
                                user_id: authUser.id,
                                currency:response.data.currency,
                                transaction_type: 'credit',
                                transaction_date: response.data.transaction_date,
                                service: PAYSTACK_SERVICE,
                                transaction_reference: response.data.reference,
                                transaction_status: COMPLETED,
                            }
                        })
                        transaction = trans[0]
                    }
                    let token = await this.helper.generateToken(10);
                    let gift_card = await this.giftCardRepository.baseInsert({
                        data:{
                                amount: transaction.amount,
                                user_id: authUser.id,
                                currency: transaction.currency,
                                transaction_id: transaction.id,
                                redeem_link: `${this.config.get('server.app.frontend_uri')}redeem/${token}`,
                                redeem_token: token,
                                redeem_status: "pending",
                        }
                    });
                    message = 'your gift card has been sent to the provided email address';
                    await this.notificationService.sendEmail({
                        templatePath:'gift_card_mail',
                        data:{
                            ...gift_card[0],
                            name: authUser.name, 
                            title:'Gift card'
                        },
                        email: authUser.email,
                        title:'Gift card',
                        subject:'Freddy gift card'
                    });
                } else {
                    await this.paymentRepository.baseUpdate({
                        where:{ id: { _eq: card.id } },
                        _set:{
                            transactions: card.transactions++
                        }
                    })
                }
            } else {
                message = 'this card is not re-useable, please use another card'
            }
        }
        return {message}
    }

    async paystackChargeCard({body, authUser}) {

        // let { id } = this.currentUser;
        // let user = await this.userRepository.baseGetOne({
        //     where:{ id: { _eq: id } }
        // });

        let { card_id, amount } = body;
        let message = ''
        let card = await this.paymentRepository.baseGetOne({
            where:{ id: { _eq: card_id } }
        });

        if(!card) {
            throw new Error('this card was not found');
        }
        // let amount = Number(data.amount) * 380
        const response = await Paystack.charge.charge({
            amount: parseInt(amount) * 100,
            authorization_code: card.authorization_code,
            email: authUser.email
        });

        if (!response || !response.data) {
            throw new Error('Could not charge this card')
        }
        let result;
        if(response.data.status == 'open_url') {
            await get(response.data.url)
            result = await Paystack.transaction.verify(
                {reference: response.data.reference}
            )
            if(!result || !result.data) {
                throw new Error('could not charge this card');
            }
            if(result.data.status == 'success') {
                let transaction = await this.transsactionRepository.baseGetOne({
                    where:{ transaction_reference: { _eq: response.data.reference } }
                });
                if(!transaction) {
                    let trans = await this.transactionRepository.baseInsert({
                        data:{
                            source:card.id,
                            amount: response.data.amount,
                            user_id: authUser.id,
                            currency:"NGN",
                            transaction_type: 'credit',
                            transaction_date: response.data.transaction_date,
                            service: PAYSTACK_SERVICE,
                            transaction_reference: response.data.reference,
                            transaction_status: COMPLETED,
                        }
                    })
                    transaction = trans[0]
                }
                let token = await this.helper.generateToken(10);
                    let gift_card = await this.giftCardRepository.baseInsert({
                        data:{
                                amount: response.data.amount,
                                user_id: authUser.id,
                                currency: "NGN",
                                transaction_id: transaction.id,
                                redeem_link: token,
                                redeem_token: `${this.config.get('server.app.frontend_uri')}redeem/${token}`,
                                redeem_status: "pending",
                        }
                    });
                    message = 'your gift card has been sent to the provided email address';

                    await this.notificationService.sendEmail({
                        templatePath:'gift_card_mail',
                        data:{...gift_card, title: 'Gift Card'},
                        email: email,
                        title:'Gift card',
                        subject:'Freddy gift card'
                    });

                await this.paymentRepository.baseUpdate({
                    where:{ id: { _eq: card.id } },
                    _set: { transactions: card.transactions + 1 }
                });
            }
        }
        return {
            message
        }
    }

    async initStripeCard({query, authUser}) {
        try {
			const customer = await Stripe.customers.create()
			const intent = await Stripe.paymentIntents.create({
				amount: 100,
				currency: 'usd',
				customer: customer.id,
				setup_future_usage: 'off_session'
			})
			return {
				stripePaymentKey: intent.client_secret,
				amount: intent.amount,
				stripePaymentIntent: intent.id,
				stripeCustomerId: intent.customer
			}
		} catch (error) {
			throw error
		}
    }

    async stripeCompleteInitCard({body: { intent, method, user_id }, authUser}) {

        // let { id } = this.currentUser;
        // let user = await this.userRepository.baseGetOne({
        //     where:{ id: {_eq: id} }
        // });
        const paymentIntent = await Stripe.paymentIntents.retrieve(intent);
        if(paymentIntent.status === 'succeeded') {
            const paymentMethod = await Stripe.paymentMethods.retrieve(method);

            let card = await this.paymentRepository.baseGetOne({
                where: {
                    _and:[
                        {last_4_digits: { _eq: auth.last4 } },
                        {exp_year: { _eq: auth.exp_year } },
                        {exp_month: { _eq: auth.exp_month} },
                        {user_id: { _eq: authUser.id } },
                        {service: { _eq: SERVICES.PAY_STACK_SERVICE } }
                    ]
                }
            })

            let refund = await Stripe.refunds.create({
                payment_intent: intent
            });

            if(!card) {
                card = await this.paymentRepository.baseInsert({
                    data: {
                        last_4_digits:paymentMethod.card.last4,
                        card_type:paymentMethod.card.brand,
                        authorization_code:method,
                        exp_year: paymentMethod.card.exp_year,
                        exp_month: paymentMethod.card.exp_month,
                        holder_name:authUser.name,
                        user_id: authUser.id,
                        transactions:1
                    }
                })
            }else {
                if(card.deleted) {
                    await this.paymentRepository.baseUpdate({
                        where:{ id: { _eq: card.id } },
                        _set:{
                            deleted:false
                        }
                    })
                }
            }
            return {
                message: 'done'
            }
        }
    }

    async stripeChargeCard({body:{amount, card_id}, query, params, authUser}) {

        let card = await this.paymentRepository.baseGetOne({
            where:{ id: { _eq: card_id } }
        });

        amount * 100
		const response = await Stripe.paymentIntents.create({
			amount: amount,
			payment_method: card.authorization_code,
			currency: 'usd',
			customer: card.customer,
			off_session: true,
			confirm: true
		})

        if(response.status === 'succeeded'){
            let transaction = await this.transactionRepository.baseGetOne({
				where: { transaction_reference: {_eq: response.id}}
			});

            if(!transaction) {
                transaction = await this.transactionRepository.baseInsert({
                    data:{
                        source:card.id,
                        amount: response.amount / 100,
                        user_id: authUser.id,
                        currency:"NGN",
                        transaction_type: 'credit',
                        transaction_date: new Date(),
                        service: STRIPE_SERVICE,
                        transaction_reference: response.id,
                        transaction_status: COMPLETED,
                    }
                })
            }
            let token = await this.helper.generateToken(10);
                    let gift_card = await this.giftCardRepository.baseInsert({
                        data:{
                                amount: response.data.amount,
                                user_id: authUser.id,
                                currency: "NGN",
                                transaction_id: transaction.id,
                                redeem_link: token,
                                redeem_token: `${this.config.get('server.app.frontend_uri')}redeem/${token}`,
                                redeem_status: "pending",
                        }
                    });
                    message = 'your gift card has been sent to the provided email address';

                    await this.notificationService.sendEmail({
                        templatePath:'gift_card_mail',
                        data:{...gift_card, title: 'Gift Card'},
                        email: email,
                        title:'Gift card',
                        subject:'Freddy gift card'
                    });

                await this.paymentRepository.baseUpdate({
                    where:{ id: { _eq: card.id } },
                    _set: { transactions: card.transactions + 1 }
                });

        }
    }

    async deleteCard({body:{card_ids}, authUser}) {
        try {
            for(let card_id of card_ids) {
                await this.paymentRepository.baseDelete({
                    where:{ id: { _eq: card_id } }
                });
            }
            return "Opperation successful"
        } catch (error) {
            throw new Error(error)
        }
    }

    async fetchBanks({query}) {
        const banks = await Paystack.misc.list_banks();
        return banks
    }


    async buyGiftyCard({body:{email, phone_number, amount, transaction_reference}}) {
        let message = "";

        // let { transaction_reference, email } = body;
        const response = await Paystack.transaction.verify({
            reference: transaction_reference
        });

        if(response.data && response.data.status == 'success') {

            if(response.data.authorization && response.data.authorization.reusable) {
                let auth = response.data.authorization;
                
                let transaction = await this.transactionRepository.baseGetOne({
                    where:{ transaction_reference: { _eq: response.data.reference } }
                });
                let random_token = await this.helper.generateToken(10);
                if(!transaction) {
                    let trans = await this.transactionRepository.baseInsert({
                        data:{
                            source:"card",
                            amount: response.data.amount / 100,
                            user_id: response.data.customer.customer_code,
                            currency:response.data.currency,
                            transaction_type: 'credit',
                            transaction_date: response.data.transaction_date,
                            service: PAYSTACK_SERVICE,
                            transaction_reference: response.data.reference,
                            transaction_status: COMPLETED,
                        }
                    })
                    transaction = trans[0]
                }

                let token = await this.helper.generateToken(10);
                let gift_card = await this.giftCardRepository.baseInsert({
                    data:{
                            amount: transaction.amount,
                            user_id: response.data.customer.customer_code,
                            currency: transaction.currency,
                            transaction_id: transaction.id,
                            redeem_link: `${this.config.get('server.app.frontend_uri')}redeem/${token}`,
                            redeem_token: token,
                            redeem_status: "pending",
                    }
                });
                message = 'your gift card has been sent to the provided email address';
                await this.notificationService.sendEmail({
                    templatePath:'gift_card_mail',
                    data:{
                        ...gift_card[0],
                        name: "annanymous", 
                        title:'Gift card'
                    },
                    email: email,
                    title:'Gift card',
                    subject:'Freddy gift card'
                });
            } else {
                message = 'this card is not re-useable, please use another card'
            }
        }
        return {message}
    }

}

module.exports = PaymentFactory;