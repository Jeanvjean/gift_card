

class GiftCardTypesFactory {
    constructor(props) {
        this.config = props.config;
        this.errors = props.errors;
        this.database = props.database;
        this.queries = props.queries;
        this.helper = props.helper;
        this.giftCardTypesRepository = props.giftCardTypesRepository;
    }

    async create({body:{
        name,
        min_price,
        max_price,
        currency
    }}) {
        try {
            const foundCard = await this.giftCardTypesRepository.baseGetOne({
                where:{name:{_eq: name}}
            });
            if(foundCard) {
                throw new Error('this card already exists')
            }
            const card = await this.giftCardTypesRepository.baseInsert({
                data:{
                    name,
                    min_price,
                    max_price,
                    currency
                }
            });
            return {
                data:card,
                message: 'card created successfully'
            }
        } catch (e) {
            throw new Error(e)
        }
    }

    async fetchCards({query:{
        name,
        min_price,
        max_price,
        currency,
        page
    }}){
        try {
            const cards = await this.giftCardTypesRepository.baseGetAll({
                columns:`*`,
                where:{
                    _and:[
                        { name: { _eq: name } },
                        { min_price: { _eq: min_price } },
                        { max_price: { _eq: max_price } }
                    ]
                },
                page
            });
            return {
                total: cards.length,
                data: cards,
                message:'cards fetched'
            }
        } catch (e) {
            throw new Error(e);
        }
    }

    async cardDetails({params:{
        card_id
    }}){
       try {
        const card = await this.giftCardTypesRepository.baseGetOne({
            where:{ id: { _eq: card_id } }
        });
        if(!card) {
            throw new Error('No card with this id was found')
        }
        return {
            ...card
        }
       } catch (e) {
           throw new Error(e)
       }
    }

    async updateCard({body, params}) {
        try {
            let { card_id } = params;
            let updateData = await this.giftCardTypesRepository.baseUpdate({
                where:{ id: {_eq: card_id} },
                _set:{...body}
            });
            return updateData;
        } catch (e) {
            throw new Error(e)
        }
    }

    async deleteCard({params:card_id}){
        await this.giftCardTypesRepository.baseDelete({
            where:{ id:{ _eq: card_id } }
        });
        return {
            message: `Successful`
        }
    }
}

module.exports = GiftCardTypesFactory;