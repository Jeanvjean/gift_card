const Paystack = require('paystack-api')(process.env.PAYSTACK_SECRET_KEY);
import { get, post } from 'axios';
import {
    COMPLETE_REDEEDM_STATUS,
    PENDING_REDEEM_STATUS,
    COMPLETED,
    PAYSTACK_SERVICE
} from '../constants'

class GiftCardsFactory {
    constructor(props) {
        this.config = props.config;
        this.errors = props.errors;
        this.database = props.database;
        this.queries = props.queries;
        this.helper = props.helper;
        this.giftCardRepository = props.giftCardRepository;
        this.transactionRepository = props.transactionRepository;
        this.recipientRepository = props.recipientRepository;
    }
        
    async fetchGiftCards({query:{
        page,
        status
    }}) {
        const t = await this.database.tx((t)=>{

            let cards = this.giftCardRepository.baseGetAll({
                where:{
                    _and:[
                        {redeem_status: { _eq: status } }
                    ]
                },
                join:[
                    { gift_card: 'user_id', users:'id' },
                    { gift_card: 'transaction_id', transactions:'id' }
                ],
                page,
                orderBy:{ created_at: 'desc' },
                transaction:t
            });
            let total = this.giftCardRepository.baseGetAll({
                columns:`COUNT(id)`,
                transaction:t
            })
            return t.batch([
                cards,
                total
            ]);
        })
        let [
            cards,
            [{count: total}]
        ] = t;

        return {
            data: cards,
            total
        }
    }

    async redeemGiftCard({body:{ account_number, bank_code, redeem_token }}){

        let card = await this.giftCardRepository.baseGetOne({
            where:{ redeem_token: { _eq: redeem_token } }
        });

        if(!card) {
            throw new Error('gift card was not found')
        }

        if(card.redeem_status == COMPLETE_REDEEDM_STATUS) {
            throw new Error('this card has been redeemed')
        }

        let recipient = await this.recipientRepository.baseGetOne({
            where:{
                _and:[
                    {'recipient.account_number': { _eq: account_number } },
                    { 'recipient.bank_code': { _eq: bank_code } }
                ]
            }
        });

        if(!recipient) {
            //Resolve account and bank
            let account = await Paystack.verification.resolveAccount({
                account_number,
                bank_code
            });

            if(account.status) {
                //TO-DO register new recipient
                let result = await Paystack.transfer_recipient.create({
                    account_number,
                    bank_code,
                    type:'nuban',
                    name:account.account_name
                });
                
                if(result.status) {
                    let bank_details = result.data.details;
                    let new_recipient = await this.recipientRepository.baseInsert({
                        data:{
                            recipient_name: bank_details.account_name,
                            bank_name: bank_details.bank_name,
                            bank_code: bank_details.bank_code,
                            account_name: bank_details.account_name,
                            currency: result.data.currency,
                            account_number: bank_details.account_number,
                            recipient_code: result.data.recipient_code,
                        }
                    })
                    recipient = new_recipient[0];
                }else {
                    throw new Error('failed to register recipient')
                }
            } else {
                throw new Error('could not resolve this account')
            }
           
        }
        //TO-DO make payment
        let transfer = await Paystack.transfer.create({
            source: "balance", 
            reason: "Redeem gift card", 
            amount: card.amount * 100, 
            recipient: recipient.recipient_code
        });

        if(transfer.data.status == 'success') {
            await this.giftCardRepository.baseUpdate({
                where:{ id: { _eq: card.id } },
                _set:{
                    redeem_status: COMPLETE_REDEEDM_STATUS
                }
            });
            await this.transactionRepository.baseInsert({
                data:{
                    source:card.id,
                    amount: transfer.data.amount / 100,
                    user_id: card.user_id,
                    currency:transfer.data.currency,
                    transaction_type: transfer.data.reason,
                    transaction_date: new Date().toDateString(),
                    service: PAYSTACK_SERVICE,
                    transaction_reference: transfer.data.reference,
                    transaction_status: COMPLETED,
                }
            });
        }

        return 'Successful';
    }

}

module.exports = GiftCardsFactory;