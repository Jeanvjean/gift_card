class AppFactory {
  constructor({ config }) {
    this.config = config;
  }
  getApp() {
    return { domain: this.config.get('server.app.domain') };
  }

  /*generator*/
}

module.exports = AppFactory;
