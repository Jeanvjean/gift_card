

class TransactionFactory {
    constructor(props) {
        this.config = props.config;
        this.errors = props.errors;
        this.database = props.database;
        this.queries = props.queries;
        this.helper = props.helper;
        this.transactionRepository = props.transactionRepository;
    }

    async deleteTransactions({body:{transactions_id}, authUser}) {
        for(let trans_id of transactions_id) {
            await this.transactionRepository.baseDelete({
                where:{ id: { _eq: trans_id} }
            })
        }
        return 'Successful';
    }

    async fetchTransactions({query:{ status, source, amount}}) {
        const transactions = await this.transactionRepository.baseGetAll({
            where:{
                _and:[
                    {'transactions.source': { _eq: source } },
                    { 'transactions.amount': {_eq: amount} },
                    { 'transactions.status' : {_eq: status} }
                ]
            }
        });

        return {
            total:transactions.length,
            data: transactions
        }
    }

    async transactionDetails({params: { transaction_id }}) {
        const transaction = await this.transactionRepository.baseGetOne({
            where:{ id: { _eq: transaction_id} }
        })

        return transaction;
    }
}

module.exports = TransactionFactory;