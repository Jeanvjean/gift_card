import { asValue } from 'awilix';
import { inject } from 'awilix-express';
import ResponseTransformer from '../utils/ResponseTransformer';

module.exports = inject(
  ({ database, queries, helper, errors, userRepository }) =>
    async (req, res, next) => {
      try {
        const bearerToken = req.headers.authorization;

        if (!bearerToken) {
          return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Invalid Bearer token')
          );
        }

        const token = bearerToken.split(' ')[1];
        if (!token) {
          return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Invalid Token')
          );
        }

        const decrypted = await helper.verifyToken(token);

        // console.log(decrypted);
        if (!decrypted) {
          return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Invalid Token')
          );
        }

        const {
          data: { id, email },
          iat,
          exp,
        } = decrypted;

        const user =
          await userRepository.fetchUserByEmail({
            email: email.toLowerCase()
          });

        // console.log(user);

        req.container.register({
          currentUser: asValue({...user}),
        });

        req.authUser = user;

        next();
      } catch (err) {
        return ResponseTransformer.error(
          req,
          res,
          new errors.Unauthorize('Authorization Error')
        );
      }
    }
);
