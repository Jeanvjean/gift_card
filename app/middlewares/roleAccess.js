const { 
    USER_TYPE_SUPER_ADMIN, 
    USER_TYPE_ADMIN, 
    USER_TYPE_CLIENT 
} = require('../constants')

const ResponseTransformer = require('../utils/ResponseTransformer');

const errors = require('../tools/errors')

export const superAdmin = async(req, res, next)=>{
    let user = req.authUser
    // console.log(user)
    if(user.user_type == USER_TYPE_SUPER_ADMIN) {        
        next();
    }else {
        return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Access denied')
        );
    }
}

export const admin = async(req, res, next)=>{
    let user = req.authUser
    if(user.user_type == USER_TYPE_ADMIN || user.user_type == USER_TYPE_SUPER_ADMIN) {        
        next();
    }else {
        return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Access denied')
        );
    }
}