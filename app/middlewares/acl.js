import { asValue } from 'awilix';
import { inject } from 'awilix-express';
import ResponseTransformer from '../utils/ResponseTransformer';

module.exports = (permission) =>
  inject(
    ({
        database,
        queries,
        helper,
        errors,
        roleRepository,
        rolePermissionRepository,
        currentUser,
      }) =>
      async (req, res, next) => {
        const { role_id } = currentUser;
				try {
          const { permissions, editable, id } = await roleRepository.baseGetOne(
            {
              columns: 'permissions, editable, id',
              where: { id: { _eq: role_id } },
            }
          );

          if (editable === '1') {
            const [{ count }] = await rolePermissionRepository.baseGetAll({
              columns: 'COUNT(role_permission.id)',
              where: {
                _and: [
                  { 'role_permission.role_id': { _eq: role_id } },
                  { 'permission.permission': { _eq: permission } },
                ],
              },
              join: [{ role_permission: 'permission_id', permission: 'id' }],
            });
						if (count === 0) {
              // throw new Error();
            }

            // return ResponseTransformer.error(
            //   req,
            //   res,
            //   new errors.Unauthorize('Authorization Error')
            // );
          }

          next();
				} catch (err) {
          console.log({ err });
					return ResponseTransformer.error(
            req,
            res,
            new errors.Unauthorize('Authorization Error')
          );
        }
      }
  );
