const DOSpacesConfig = {
  secretKey: process.env.DIGITAL_OCEAN_SPACES_SECRET_KEY,
  accessKey: process.env.DIGITAL_OCEAN_SPACES_ACCESS_KEY,
  bucket: process.env.DIGITAL_OCEAN_SPACES_BUCKET,
  url: process.env.DIGITAL_OCEAN_SPACES_URL,
  region: process.env.DIGITAL_OCEAN_SPACES_REGION,
};

module.exports = { DOSpacesConfig };
