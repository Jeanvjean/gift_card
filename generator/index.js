const generateFileByType = require('./generateFileByType');
const { getArguments } = require('./getArguments');
const options = getArguments();
generateFileByType(options);

// console.log(process.argv, data);
/**
 *
 * npm run generate type={crud/repository/controller/factory/route/endpoint} name={name of the module (app for AppController,AppFactory etc.)} fields="name,phonenumber" route="for endpoint alone"
 */
