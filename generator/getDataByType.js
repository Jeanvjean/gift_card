const {
  getRepositoryData,
  getControllerData,
  getFactoryData,
  getRouteData,
  getSchemaData,
  getEndpointData,
} = require('./getData');

const getDataByType = ({ type, ...others }) => {
  switch (type) {
    case 'repository':
      return getRepositoryData({ type, ...others });
    case 'controller':
      return getControllerData({ type, ...others });
    case 'factory':
      return getFactoryData({ type, ...others });
    case 'route':
      return getRouteData({ type, ...others });
    case 'schema':
      return getSchemaData({ type, ...others });
    case 'endpoint':
      return getEndpointData(type, ...others);
  }
};

module.exports = getDataByType;
