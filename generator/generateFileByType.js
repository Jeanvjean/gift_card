const { paths, templatePath } = require('./config');
const generateFile = require('./generateFile');
const getDataByType = require('./getDataByType');
const generateControllerEndpointFile = require('./generateFile/generateControllerEndpointFile');
const generateFactoryEndpointFile = require('./generateFile/generateFactoryEndpointFile');
const generateSchemaEndpointFile = require('./generateFile/generateSchemaEndpointFile');
const generateRouteEndpointFile = require('./generateFile/generateRouteEndpointFile');

const generateFileByType = ({ type, ...others }) => {
  switch (type) {
    case 'repository':
      generateFile({
        destPath: paths.repository,
        templatePath: templatePath.repository,
        data: getDataByType({ type, ...others }),
      });
      break;
    case 'controller':
      generateFile({
        destPath: paths.controller,
        templatePath: templatePath.controller,
        data: getDataByType({ type, ...others }),
      });
      break;
    case 'factory':
      generateFile({
        destPath: paths.factory,
        templatePath: templatePath.factory,
        data: getDataByType({ type, ...others }),
      });
      break;
    case 'route':
      generateFile({
        destPath: paths.route,
        templatePath: templatePath.route,
        data: getDataByType({ type, ...others }),
      });
      break;
    case 'crud':
      generateFile({
        destPath: paths.route,
        templatePath: templatePath.route,
        data: getDataByType({ type: 'route', ...others }),
      });
      generateFile({
        destPath: paths.repository,
        templatePath: templatePath.repository,
        data: getDataByType({ type: 'repository', ...others }),
      });
      generateFile({
        destPath: paths.factory,
        templatePath: templatePath.factory,
        data: getDataByType({ type: 'factory', ...others }),
      });
      generateFile({
        destPath: paths.controller,
        templatePath: templatePath.controller,
        data: getDataByType({ type: 'controller', ...others }),
      });
      generateFile({
        destPath: paths.schema,
        templatePath: templatePath.schema,
        data: getDataByType({ type: 'schema', ...others }),
      });
      break;
    case 'endpoint':
      generateControllerEndpointFile(
        getDataByType({ type: 'controller', ...others })
      );
      generateFactoryEndpointFile(
        getDataByType({ type: 'factory', ...others })
      );
      generateSchemaEndpointFile(getDataByType({ type: 'schema', ...others }));
      generateRouteEndpointFile(getDataByType({ type: 'route', ...others }));

      break;
  }
};

module.exports = generateFileByType;
