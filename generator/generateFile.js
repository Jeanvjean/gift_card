const { renderFile } = require('template-file');
const basePath = require('path').resolve('./');
const fs = require('fs');
const generateFile = async ({ destPath, templatePath, data }) => {
  try {
    const template = await renderFile(`${basePath}${templatePath}`, data);
    fs.writeFileSync(`${basePath}${destPath}${data.fileName}`, template);
    // console.log(`${basePath}${destPath}${data.fileName}`);
    // console.log(template);
  } catch (e) {
    console.log(e);
  }
};

module.exports = generateFile;
