function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}
function unCapitalizeFirstLetter(string) {
  return string.charAt(0).toLowerCase() + string.slice(1);
}

const getRepositoryData = ({ type, name, ...others }) => {
  return {
    repositoryName: name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join(''),
    fileName: `${name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join('')}Repository.js`,
    tableName: name,
    ...others,
    type,
  };
};

const getControllerData = ({ type, name, ...others }) => {
  return {
    controllerName: name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join(''),
    fileName: `${name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join('')}Controller.js`,
    factoryName: unCapitalizeFirstLetter(
      `${name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')}`
    ),
    ...others,
    type,
  };
};

const getFactoryData = ({ type, name, ...others }) => {
  return {
    factoryName: name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join(''),
    fileName: `${name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join('')}Factory.js`,
    repositoryName: unCapitalizeFirstLetter(
      `${name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')}`
    ),
    ...others,
    type,
  };
};

const getRouteData = ({ type, name, ...others }) => {
  return {
    controllerName: name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join(''),
    schemaFileName: `${unCapitalizeFirstLetter(
      name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')
    )}`,
    fileName: `${unCapitalizeFirstLetter(
      name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')
    )}.js`,
    routeName: unCapitalizeFirstLetter(
      name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')
    ),
    ...others,
    type,
  };
};

const getSchemaData = ({ type, name, ...others }) => {
  return {
    controllerName: name
      .split('_')
      .map((item) => capitalizeFirstLetter(item))
      .join(''),
    fileName: `${unCapitalizeFirstLetter(
      name
        .split('_')
        .map((item) => capitalizeFirstLetter(item))
        .join('')
    )}.js`,

    ...others,
    type,
  };
};

module.exports = {
  getRepositoryData,
  getControllerData,
  getFactoryData,
  getRouteData,
  getSchemaData,
};
