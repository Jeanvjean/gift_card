const getArguments = () => {
  const output = {};
  process.argv.forEach((item) => {
    let spliteItem = item.split('=');
    if (spliteItem.length === 2) {
      let [key, value] = spliteItem;
      Object.assign(output, {
        [key]:
          key === 'fields' ? value.split(',').map((f) => ({ name: f })) : value,
      });
    }
  });
  if (!output.fields) {
    Object.assign(output, { fields: [] });
  }
  return output;
};
module.exports = { getArguments };
