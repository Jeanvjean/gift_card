const basePath = require('path').resolve('./');
const fs = require('fs');
const { renderFile } = require('template-file');

module.exports = async (data) => {
  const factory = fs
    .readFileSync(`${basePath}/app/factories/${data.factoryName}Factory.js`)
    .toString();
  let splitString = `/*generator*/`;

  let splitContent = factory.split(splitString);

  const template = await renderFile(
    `${basePath}/generator/template/endpoint/factory.template`,
    data
  );
  splitContent[0] = `${splitContent[0]}
  ${template}
  `;
  fs.writeFileSync(
    `${basePath}/app/factories/${data.factoryName}Factory.js`,
    splitContent.join(splitString)
  );
};
