const basePath = require('path').resolve('./');
const fs = require('fs');
const { renderFile } = require('template-file');

module.exports = async (data) => {
  const factory = fs
    .readFileSync(`${basePath}/app/routes/${data.fileName}`)
    .toString();
  let splitString = `/*generator*/`;

  let splitContent = factory.split(splitString);

  const template = await renderFile(
    `${basePath}/generator/template/endpoint/route.template`,
    data
  );
  splitContent[0] = `${splitContent[0]}
  ${template}
  `;
  let newContent = splitContent.join(splitString);
  let spliteForExport = newContent.split('/*importerGenerator*/');
  spliteForExport[0] = `${spliteForExport[0]}${data.route}Schema,
  `;
  newContent = spliteForExport.join('/*importerGenerator*/');

  fs.writeFileSync(`${basePath}/app/routes/${data.fileName}`, newContent);
};
