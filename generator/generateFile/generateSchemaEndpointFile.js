const basePath = require('path').resolve('./');
const fs = require('fs');
const { renderFile } = require('template-file');

module.exports = async (data) => {
  const factory = fs
    .readFileSync(`${basePath}/app/validations/${data.fileName}`)
    .toString();
  let splitString = `/*generator*/`;

  let splitContent = factory.split(splitString);

  const template = await renderFile(
    `${basePath}/generator/template/endpoint/schema.template`,
    data
  );
  splitContent[0] = `${splitContent[0]}
  ${template}
  `;
  let newContent = splitContent.join(splitString);
  let spliteForExport = newContent.split('/*exporterGenerator*/');
  spliteForExport[0] = `${spliteForExport[0]}${data.route}Schema,
  `;
  newContent = spliteForExport.join('/*exporterGenerator*/');

  fs.writeFileSync(`${basePath}/app/validations/${data.fileName}`, newContent);
};
