const basePath = require('path').resolve('./');
const fs = require('fs');
const { renderFile } = require('template-file');

module.exports = async (data) => {
  const controller = fs
    .readFileSync(
      `${basePath}/app/controllers/${data.controllerName}Controller.js`
    )
    .toString();
  let splitString = `/*generator*/`;

  let splitContent = controller.split(splitString);

  const template = await renderFile(
    `${basePath}/generator/template/endpoint/controller.template`,
    data
  );
  splitContent[0] = `${splitContent[0]}
  ${template}
  `;
  fs.writeFileSync(
    `${basePath}/app/controllers/${data.controllerName}Controller.js`,
    splitContent.join(splitString)
  );
};
