const paths = {
  repository: '/app/repositories/',
  controller: '/app/controllers/',
  factory: '/app/factories/',
  route: '/app/routes/',
  schema: '/app/validations/',
};

const generate = {
  repository: ['repository'],
  crud: ['controller', 'factory', 'route'],
};

const templatePath = {
  repository: '/generator/template/repository.template',
  controller: '/generator/template/controller.template',
  factory: '/generator/template/factory.template',
  route: '/generator/template/route.template',
  schema: '/generator/template/schema.template',
};

module.exports = { paths, generate, templatePath };
