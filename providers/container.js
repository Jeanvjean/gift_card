const config = require('../config');
const errors = require('../app/tools/errors');
const queries = require('../app/queries');
const database = require('../app/utils/database');
const helper = require('./../app/utils/Helper');
const ResultCacheQueue = require('../app/utils/ResultCacheQueue');

module.exports = {
  boot(app, { awilix }) {
    const { asValue, asClass, createContainer } = awilix;
    const container = createContainer();
    container.loadModules(
      [
        // Globs!
        'app/factories/**/*.js',
        'app/controllers/**/*.js',
        'app/services/**/*.js',
        'app/repositories/**/*.js',
      ],
      {
        formatName: 'camelCase',
        registrationOptions: {
          // lifetime: awilix.Lifetime.SINGLETON
        },
      }
    );
    container.register({
      config: asValue(config),
      queries: asValue(queries),
      errors: asValue(errors),
      database: asValue(database),
      helper: asClass(helper),
      resultUploadQueue: asClass(ResultCacheQueue).singleton(),
      currentUser: asValue(null),
    });
    return container;
  },
};
